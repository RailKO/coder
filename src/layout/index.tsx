import { Container } from '@mui/material';
import { Box } from '@mui/system';
import React, { FC } from 'react';
import { URLS } from '../__data__/constants';
import Footer from './components/Footer';
import Header, { MenuItem } from './components/Header';
import { useTranslation } from 'react-i18next';

const AppLayout: FC = ({ children }) => {
    const { t } = useTranslation();

    const menu: Array<MenuItem> = [
        { title: t('main'), url: URLS.base, isMain: true },
        { title: t('catalog'), url: URLS.catalog, isMain: false },
        { title: t('forum'), url: URLS.forum, isMain: false },
        { title: t('detail'), url: URLS.topic, isMain: false },
    ];

    return (
        <Box bgcolor="grey.100">
            <Container>
                <Box minHeight="100vh" display="flex" flexDirection="column">
                    <Header pages={menu} />
                    <Box pt={10} minWidth={1100}>
                        <Container component="main">{children}</Container>
                    </Box>
                    <Footer />
                </Box>
            </Container>
        </Box>
    );
};

export default AppLayout;
