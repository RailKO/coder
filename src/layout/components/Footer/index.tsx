import { Box } from '@mui/system';
import React, { FC } from 'react';

const Footer: FC = () => {
    return <Box component="footer" height="50px" bgcolor="white" mt="auto"></Box>;
};

export default Footer;
