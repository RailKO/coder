import React, { FC } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { logoIcon } from '../../../assets';
import { AppBar, Avatar, Box, Container, MenuItem, Stack, Toolbar, Typography } from '@mui/material';
import { Button } from './styles';
import { URLS } from '../../../__data__/constants';
import { useTranslation } from 'react-i18next';

export type MenuItem = {
    title: string;
    url: string;
    isMain: boolean;
};

type PropsType = {
    pages: Array<MenuItem>;
};

const Header: FC<PropsType> = ({ pages }) => {
    const mainPage = pages.find((item) => item.isMain);
    const navigate = useNavigate();

    const { t, i18n } = useTranslation();

    const handleChangeLangToEn = async () => {
        await i18n.changeLanguage('en');
    };

    const handleChangeLangToRu = async () => {
        await i18n.changeLanguage('ru');
    };

    return (
        <AppBar>
            <Container>
                <Toolbar>
                    <Box mr={2}>
                        {mainPage && (
                            <Link to={mainPage.url}>
                                <Avatar alt="site logo" src={logoIcon} variant="square" />
                            </Link>
                        )}
                    </Box>
                    <Box justifyContent="space-between" display="flex" width="100%">
                        <Box>
                            <nav>
                                {pages &&
                                    pages.map((page) => {
                                        return page.isMain ? null : (
                                            <Button
                                                key={page.title}
                                                variant="text"
                                                color="secondary"
                                                onClick={() => {
                                                    navigate(page.url);
                                                }}
                                            >
                                                <Typography>{page.title}</Typography>
                                            </Button>
                                        );
                                    })}
                            </nav>
                        </Box>
                        <Stack direction="row">
                            <Box>
                                <Button
                                    variant="text"
                                    color="secondary"
                                    aria-label={t('sign_up')}
                                    onClick={() => {
                                        navigate(URLS.auth.urlSignUp);
                                    }}
                                >
                                    <Typography>{t('sign_up')}</Typography>
                                </Button>
                                <Button
                                    variant="text"
                                    color="secondary"
                                    aria-label={t('sign_in')}
                                    onClick={() => {
                                        navigate(URLS.auth.urlSignIn);
                                    }}
                                >
                                    <Typography>{t('sign_in')}</Typography>
                                </Button>
                            </Box>
                            <Box>
                                <Button variant="text" aria-label="EN" color="secondary" onClick={handleChangeLangToEn} size="small">
                                    <Typography>EN</Typography>
                                </Button>
                                <Button variant="text" aria-label="RU" color="secondary" onClick={handleChangeLangToRu}>
                                    <Typography>RU</Typography>
                                </Button>
                            </Box>
                        </Stack>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default Header;
