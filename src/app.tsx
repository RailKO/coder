import React, { useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AppLayout from './layout';
import { URLS } from './__data__/constants';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { ApiProvider } from '@reduxjs/toolkit/query/react';
import { api } from './__data__/services/api';
import ErrorBoundary from './components/error-boundary/error-boundary';
import {
    CatalogComponent,
    ForumPage,
    NewTopicPage,
    NotFoundPage,
    TopicListPage,
    ViewTopicPage,
    AuthSignUp,
    AuthSignIn,
    AuthPasswRst,
    AuthPasswRstLogged,
    AuthPasswMessage,
} from './pages';
import AsideComponent from './pages/detail/index';
import { theme } from 'theme/theme';

const App = () => {
    useEffect(() => {
        document.title = 'Coder';
    }, []);

    return (
        <ApiProvider api={api}>
            <BrowserRouter>
                <CssBaseline />
                <ThemeProvider theme={theme}>
                    <ErrorBoundary>
                        <AppLayout>
                            <Routes>
                                <Route path={URLS.forum} element={<ForumPage />} />
                                <Route path={URLS.forumGroup} element={<TopicListPage />} />
                                <Route path={URLS.forumNewTopic} element={<NewTopicPage />} />
                                <Route path={URLS.forumTopic} element={<ViewTopicPage />} />
                                <Route path={URLS.base} element={<CatalogComponent />} />

                                <Route path={URLS.topic} element={<AsideComponent />} />
                                <Route path={URLS.topicSection} element={<AsideComponent />} />
                                <Route path={URLS.topics} element={<CatalogComponent />} />

                                <Route path={URLS.auth.urlSignUp} element={<AuthSignUp />} />
                                <Route path={URLS.auth.urlSignIn} element={<AuthSignIn />} />
                                <Route path={URLS.auth.urlResetPassw} element={<AuthPasswRst />} />
                                <Route path={URLS.auth.urlResetPasswMessage} element={<AuthPasswMessage />} />
                                <Route path={URLS.auth.urlResetPasswMessageLogged} element={<AuthPasswRstLogged />} />
                                <Route path="*" element={<NotFoundPage />} />
                            </Routes>
                        </AppLayout>
                    </ErrorBoundary>
                </ThemeProvider>
            </BrowserRouter>
        </ApiProvider>
    );
};

export default App;
