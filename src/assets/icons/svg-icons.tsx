import React from 'react';
import AssignmentIcon from '@mui/icons-material/Assignment';
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import StarBorderOutlinedIcon from '@mui/icons-material/StarBorderOutlined';
import ChatBubbleOutlineOutlinedIcon from '@mui/icons-material/ChatBubbleOutlineOutlined';
import LockIcon from '@mui/icons-material/Lock';

export const LockComponent = () => (
    <LockIcon/>
);

export const Assign = () => (
    <AssignmentIcon/>
);

export const ArrowLeft = () => (
    <ArrowLeftIcon/>
);

export const Discussion = ()=> (
    <ChatBubbleOutlineOutlinedIcon/>
);

export const EnabledComponent = ()=> (
    <CircleOutlinedIcon/>
);

export const Favorite = ()=> (
    <StarBorderOutlinedIcon/>
);
