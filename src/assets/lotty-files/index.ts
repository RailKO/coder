export { default as notFoundAnimation } from './404-page-not-found.json';
export { default as somethingWentWrongAnimation } from './something-went-wrong.json';
