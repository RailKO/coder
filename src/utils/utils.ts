export const unixToDate = (value: number) => new Date(value * 1000).toLocaleString('ru-RU', { timeZone: 'UTC' });

export const changeToKilo = (value: number): string => {
    if (value < 1000) {
        return value.toString();
    }
    return new Intl.NumberFormat('ru-RU', { maximumFractionDigits: 1 }).format(value / 1000) + 'K';
};
