import React from 'react';
import { ApiProvider } from '@reduxjs/toolkit/dist/query/react';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { theme } from '../../theme/theme';
import { api } from '../__data__/services/api';
import ErrorBoundary from '../components/error-boundary/error-boundary';
import AppLayout from '../layout';

// eslint-disable-next-line react/prop-types
const AllProviders = ({ children }) => {
    return (
        <ApiProvider api={api}>
            <BrowserRouter>
                <CssBaseline />
                <ThemeProvider theme={theme}>
                    <ErrorBoundary>
                        <AppLayout>{children}</AppLayout>
                    </ErrorBoundary>
                </ThemeProvider>
            </BrowserRouter>
        </ApiProvider>
    );
};

const customRender = (ui, options?) => render(ui, { wrapper: AllProviders, ...options });

export * from '@testing-library/react';
export { customRender as render };
