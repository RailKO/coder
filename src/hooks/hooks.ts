import { useCallback, useState } from 'react';

type PaginationType = [value: number, handler: (event: React.ChangeEvent<unknown>, value: number) => void];

export const usePagination = (initialValue: number): PaginationType => {
    const [page, setPage] = useState(initialValue);

    const handleChange = useCallback((event: React.ChangeEvent<unknown>, value: number) => {
        setPage(value);
    }, []);

    return [page, handleChange];
};
