export type SectionType = {
    title: string;
    isLock: boolean;
    slug: string;
    content: string;
};

export type ChaptersType = {
    overview: string;
    section: string;
    chapters: Array<SectionType>;
};

export type CategoryTopicListType = {
    title: string;
    overview: string;
    contentChapters: ChaptersType;
};
