export type CategoryType = {
    id: number;
    title: string;
    description: string;
    path: string;
};

export type CategoryTopicListType = {
    totalNum: number;
    items: Array<CategoryTopicType>;
};

export type CategoryTopicType = {
    id: number;
    title: string;
    commentCount: number;
    viewCount: number;
    voteCount: number;
    creationDate: number;
    author: UserType;
};

export type UserType = {
    name: string;
    avatar: string;
};

export type TagType = {
    id: number;
    name: string;
    numTopics: number;
};

export type TopicType = {
    id: number;
    title: string;
    commentCount: number;
    viewCount: number;
    tags: Array<TagType>;
    voteCount: number;
    voteStatus: number;
    content: string;
    updationDate: number;
    creationDate: number;
    author: UserType;
    authorIsModerator: boolean;
    isOwnPost: boolean;
};

export type CommentType = {
    id: number;
    topicId: number;
    voteCount: number;
    content: string;
    updationDate: number;
    creationDate: number;
    author: UserType;
    authorIsModerator: boolean;
    isOwnPost: boolean;
};

export type CommentToInsertType = {
    topicId: number;
    content: string;
    authorId: number;
};

export type ForumTopicToInsertType = {
    categoryId: number;
    title: string;
    tags: Array<TagType>;
    content: string;
    authorId: number;
}

export type SortItemType = {
    id: number;
    title: string;
};
