import React, { FC, useEffect } from 'react';
import '@toast-ui/editor/dist/toastui-editor.css';
import { Editor } from '@toast-ui/react-editor';
import { useTranslation } from 'react-i18next';

type PropsType = {
    initialContent: string;
    changeContent: (content: string) => void;
};

const TopicEditor: FC<PropsType> = ({ initialContent, changeContent }) => {
    const [value, setValue] = React.useState(initialContent);

    const ref = React.useRef<Editor>(null);

    const { t } = useTranslation();

    useEffect(() => {
        changeContent(value);
    }, [value]);

    const onChange = () => {
        const instance = ref.current?.getInstance();
        if (instance) {
            setValue(instance.getMarkdown());
        }
    };

    return (
        <Editor
            ref={ref}
            initialValue={value}
            previewStyle="vertical"
            height="700px"
            initialEditType="markdown"
            useCommandShortcut={true}
            placeholder={t('please_enter_text')}
            onChange={onChange}
        />
    );
};

export default TopicEditor;
