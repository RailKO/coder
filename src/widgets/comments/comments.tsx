import { Box, List, ListItem, Pagination, Stack, Typography } from '@mui/material';
import InsertCommentIcon from '@mui/icons-material/InsertComment';
import React, { FC } from 'react';
import { usePagination } from '../../hooks/hooks';
import { useGetForumTopicCommentsQuery } from '../../__data__/services/api';
import { CommentEditor, Comment, Sorting, Banner } from '../../components';
import { SortItemType } from '../../typings/types';
import { useTranslation } from 'react-i18next';
import { SkeletonComments } from '../../components/skeletons';

type PropsType = {
    topicId: number;
};

const Comments: FC<PropsType> = ({ topicId }) => {
    const [page, setPage] = usePagination(1);
    const { data, isLoading, refetch } = useGetForumTopicCommentsQuery({ topicId, page });
    const { t } = useTranslation();

    // TODO get the sort methods from back
    const sortMethods: Array<SortItemType> = [
        { id: 1, title: t('best') },
        { id: 2, title: t('most_votes') },
        { id: 3, title: t('newest_to_oldest') },
        { id: 4, title: t('oldest_to_newest') },
    ];

    if (isLoading) {
        return <SkeletonComments />;
    }

    if (!data.success) {
        return <Banner message={t('something_went_wrong')} />;
    }

    return (
        <Box>
            <Stack direction="row" bgcolor="grey.200" justifyContent="space-between">
                <Stack direction="row" alignItems="center">
                    <Box alignItems="center" display="flex" mr={1} ml={1}>
                        <InsertCommentIcon color="disabled" />
                    </Box>
                    <Typography>
                        {t('comments')} {data?.body?.comments.total}
                    </Typography>
                </Stack>
                <Sorting items={sortMethods} activeItem={sortMethods[0]} handler={(id: number) => {}} title="comments sorting" />
            </Stack>
            <Box borderBottom={1} borderColor="grey.300">
                <CommentEditor onPostComment={refetch} />
            </Box>
            <List>
                {data?.body?.comments.map((comment) => {
                    return (
                        <ListItem key={comment.id}>
                            <Comment comment={comment} />
                        </ListItem>
                    );
                })}
            </List>
            <Box justifyContent="center" alignItems="center" display="flex" p={1}>
                <Pagination count={data?.body?.totalPages} page={page} onChange={setPage} />
            </Box>
        </Box>
    );
};

export default Comments;
