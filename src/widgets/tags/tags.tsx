import React, { FC, useEffect, useState } from 'react';
import { Box, Typography } from '@mui/material';
import { useFindForumTagsMutation, useGetForumTopicTagsQuery } from '../../__data__/services/api';
import { Search, Tag } from '../../components';
import { useTranslation } from 'react-i18next';
import { SkeletonTags } from '../../components/skeletons';

const MAX_SHOWN_TAGS = 10;

const Tags: FC = () => {
    const { data, isLoading } = useGetForumTopicTagsQuery(undefined);
    const [findTags, result] = useFindForumTagsMutation();
    const [tags, setTags] = useState(null);
    const { t } = useTranslation();

    // TODO its right?
    useEffect(() => {
        if (data && result.isUninitialized) {
            setTags(data.body);
        }

        if (result.isSuccess) {
            setTags(result.data.body);
        }
    }, [data, result]);

    const searchHandler = (value: string) => {
        findTags(value);
    };

    if (isLoading) {
        return <SkeletonTags />;
    }

    if (!data.success) {
        return null;
    }

    if (!tags) {
        return null;
    }

    return (
        <Box bgcolor="primary.main" borderRadius={1}>
            <Box p={1} borderBottom={1} borderColor="grey.300">
                <Typography variant="h6" color="grey.500">
                    {t('tags')}
                </Typography>
            </Box>
            <Box p={1}>
                <Search handler={searchHandler} placeholder={t('search_for_tags')} withIcon />
            </Box>
            <Box display="flex" flexWrap="wrap" pl={1} pr={1} overflow="clip" maxHeight="50vh">
                {tags.slice(0, MAX_SHOWN_TAGS).map((tag) => (
                    <Tag key={tag.id} tag={tag} />
                ))}
            </Box>
            <Box>
                <Typography p={1} variant="body1" color="grey.500">
                    {tags.length > MAX_SHOWN_TAGS
                        ? `... ${tags.length - MAX_SHOWN_TAGS} ${t('tags_not_shown')}`
                        : t('no_more_tags_to_show')}
                </Typography>
            </Box>
        </Box>
    );
};

export default Tags;
