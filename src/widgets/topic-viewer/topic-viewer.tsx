import { Box } from '@mui/system';
import { Viewer } from '@toast-ui/react-editor';
import React, { FC } from 'react';

type PropsType = {
    content: string;
};

const TopicViewer: FC<PropsType> = ({ content }) => {
    return (
        <Box minHeight="300px" p={1}>
            <Viewer initialValue={content} />
        </Box>
    );
};

export default TopicViewer;
