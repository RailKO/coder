import React, { FC, useEffect, useState } from 'react';
import { Autocomplete, Box, createFilterOptions, TextField } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { useGetForumTopicTagsQuery } from '../../__data__/services/api';
import { SkeletonTagsPicker } from '../../components/skeletons';
import { TagType } from '../../typings/types';

type PropsType = {
    getTags: (values: Array<TagType>) => void;
};

const filter = createFilterOptions<TagType>();

const TagsPicker: FC<PropsType> = ({ getTags }) => {
    const { data, isLoading } = useGetForumTopicTagsQuery(undefined);
    const { t } = useTranslation();
    const [values, setValues] = useState<Array<TagType> | null>([]);
    let newTagName = '';

    const onChange = (event, newValue) => {
        // always is array, because is multiple
        newValue.forEach((tag) => {
            if (tag.id === -1) {
                tag.id = 0;
                tag.name = newTagName;
            }
        });
        setValues(newValue);
    };

    useEffect(() => {
        getTags(values);
    }, [values]);

    const onFilterOptions = (options: Array<TagType>, params) => {
        const filtered = filter(options, params);
        const { inputValue } = params;
        // Suggest the creation of a new value
        const isExisting = options.some((option: TagType) => inputValue === option.name);
        if (inputValue !== '' && !isExisting) {
            filtered.push({
                id: -1,
                name: `${t('create_new_tag')} "${inputValue}"`,
                numTopics: 0,
            });
            newTagName = inputValue;
        }
        return filtered;
    };

    const onGetOptionLabel = (option: TagType) => {
        return option.name;
    };

    const onOptionEqualToValue = (option, value) => {
        // TODO reimplement
        return option.name === value.name;
    };

    if (isLoading) {
        return <SkeletonTagsPicker />;
    }

    if (!data?.success) {
        // TODO return error banner
        return null;
    }

    return (
        <Box mb={1}>
            <Autocomplete
                id="tags picker"
                multiple
                value={values}
                limitTags={4}
                options={data.body}
                fullWidth
                size="small"
                placeholder={t('tag_your_topic')}
                selectOnFocus
                clearOnBlur
                handleHomeEndKeys
                isOptionEqualToValue={onOptionEqualToValue}
                renderInput={(params) => <TextField {...params} placeholder={t('tag_your_topic')} color="secondary" />}
                renderOption={(props, option) => <li {...props}>{option.name}</li>}
                onChange={onChange}
                filterOptions={onFilterOptions}
                getOptionLabel={onGetOptionLabel}
            />
        </Box>
    );
};

export default TagsPicker;
