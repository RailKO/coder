import React, { FC, useEffect } from 'react';
import { CategoryType, SortItemType } from '../../typings/types';
import { Box, Pagination, Stack, Typography } from '@mui/material';
import { usePagination } from '../../hooks/hooks';
import { createUrl } from '../../__data__/constants';
import { Banner, Search, Sorting, Topic } from '../../components';
import { Add } from '@mui/icons-material';
import { Button } from './topics.styles';
import { useLocation, useNavigate } from 'react-router-dom';
import { useGetForumTopicListQuery } from '../../__data__/services/api';
import { useTranslation } from 'react-i18next';
import { SkeletonTopicList } from '../../components/skeletons';

type PropsType = {
    currentCategory: CategoryType;
};

const Topics: FC<PropsType> = ({ currentCategory }) => {
    const [page, setPage] = usePagination(1);
    const { data, isLoading } = useGetForumTopicListQuery({ categoryId: currentCategory.id, page });
    const { t } = useTranslation();
    const navigate = useNavigate();
    const location = useLocation();

    useEffect(() => {
        setPage(undefined, 1);
    }, [location]);

    // TODO get the sort methods from back
    const sortMethods: Array<SortItemType> = [
        { id: 1, title: t('hot') },
        { id: 2, title: t('newest_to_oldest') },
        { id: 3, title: t('most_votes') },
    ];

    const newTopicUrl = createUrl('link.coder.forum.new.topic', { groupId: currentCategory.path });

    if (isLoading) {
        return <SkeletonTopicList />;
    }

    if (!data.success) {
        return <Banner message={t('topics_did_not_load')} />;
    }

    return (
        <Box bgcolor="white" borderRadius={1}>
            <Box p={1} borderBottom={1} borderColor="grey.300">
                <Stack direction="row" spacing={2} alignItems="center">
                    <Typography variant="h6">{currentCategory?.title}</Typography>
                    <Typography variant="subtitle1" color="grey.500" overflow="hidden">
                        {currentCategory?.description}
                    </Typography>
                </Stack>
            </Box>
            <Box pl={1} pr={1} borderBottom={1} borderColor="grey.300">
                <Stack direction="row" alignItems="center" justifyContent="space-between">
                    <Box>
                        <Sorting items={sortMethods} activeItem={sortMethods[0]} handler={(id: number) => {}} title="topics sorting" />
                    </Box>
                    <Box>
                        <Stack spacing={1} direction="row" alignItems="center">
                            <Search handler={() => {}} placeholder={t('search_topics')} withIcon={false} />
                            {newTopicUrl && (
                                <Button variant="contained" endIcon={<Add />} color="secondary" onClick={() => navigate(newTopicUrl)}>
                                    {t('new')}
                                </Button>
                            )}
                        </Stack>
                    </Box>
                </Stack>
            </Box>
            <Box>
                {data?.body?.topics.map((topic) => (
                    <Topic key={topic.id} topic={topic} currentCategory={currentCategory} />
                ))}
            </Box>
            <Box justifyContent="center" alignItems="center" display="flex" p={1}>
                <Pagination count={data?.body?.totalPages} page={page} onChange={setPage} />
            </Box>
        </Box>
    );
};

export default Topics;
