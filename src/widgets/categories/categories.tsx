import { Box, Stack } from '@mui/material';
import React, { FC } from 'react';
import { Banner, Category } from '../../components';
import { SkeletonCategories } from '../../components/skeletons';
import { useGetForumTopicCategoriesQuery } from '../../__data__/services/api';
import { useTranslation } from 'react-i18next';

type PropsType = {};

const Categories: FC<PropsType> = () => {
    const { data, isLoading } = useGetForumTopicCategoriesQuery(undefined);
    const { t } = useTranslation();

    if (isLoading) {
        return <SkeletonCategories />;
    }

    if (!data.success) {
        return <Banner message={t('categories_did_not_load')} />;
    }

    return (
        <Box>
            <Stack direction="row" spacing={2} justifyContent="space-between" mb={2}>
                {data?.body.map((category) => (
                    <Category key={category.id} category={category} />
                ))}
            </Stack>
        </Box>
    );
};

export default Categories;
