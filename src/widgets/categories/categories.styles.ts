import { Card as MuiCard, styled } from '@mui/material';

export const Card = styled(MuiCard)`
  display: flex;
  margin-bottom: 10px;
  justify-content: space-between;
`;
