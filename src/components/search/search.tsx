import { Box, FormControl, IconButton, InputAdornment, OutlinedInput } from '@mui/material';
import React, { FC, useState } from 'react';
import SearchIcon from '@mui/icons-material/Search';

type PropsType = {
  placeholder: string;
  handler: (value: string) => void;
  withIcon: boolean;
};

export const Search: FC<PropsType> = ({ placeholder, handler, withIcon }) => {
    const [value, setValue] = useState('');

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value);
        handler(event.target.value);
    };

    return (
        <Box>
            <FormControl variant="filled" size='small' fullWidth>
                <OutlinedInput
                    value={value}
                    onChange={handleChange}
                    color= 'secondary'
                    placeholder={placeholder}
                    endAdornment={
                        withIcon ? (
                            <InputAdornment position="end">
                                <IconButton>
                                    <SearchIcon />
                                </IconButton>
                            </InputAdornment>
                        ) : null
                    }
                />
            </FormControl>
        </Box>
    );
};

export default Search;
