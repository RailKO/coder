import { Box } from '@mui/material';
import Lottie from 'lottie-react';
import React from 'react';
import { somethingWentWrongAnimation } from '../../assets/lotty-files';

type PropsType = {
    children: any;
};

type StateType = {
    hasError: boolean;
};

/**
 * Class for catch errors
 */
class ErrorBoundary extends React.Component<PropsType, StateType> {
    /**
     *
     * @param {any} props properties
     */
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    /**
     *
     * @param {any} error caused error
     * @return {any} error state
     */
    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    /**
     *
     * @param {Error} error caused error
     * @param {React.ErrorInfo} errorInfo info by error
     */
    componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
        // console.log(error, errorInfo);
    }

    /**
     * Render error animation or children
     * @return {any} compenent
     */
    render() {
        if (this.state.hasError) {
            return (
                <Box width={500} m="0 auto">
                    <Lottie animationData={somethingWentWrongAnimation} autoPlay />
                </Box>
            );
        }
        return this.props.children;
    }
}

export default ErrorBoundary;
