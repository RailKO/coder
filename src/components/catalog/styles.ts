import styled from '@emotion/styled';
import CardMUI from '@mui/material/Card';
import BoxMUI from '@mui/material/Box';
import TypographyMUI from '@mui/material/Typography';
import CardMediaMUI from '@mui/material/CardMedia';
import CardContentMUI from '@mui/material/CardContent';
import GridMUI from '@mui/material/Grid';
import ButtonMUI from '@mui/material/Button';

export const Card = styled(CardMUI) `
    display:flex;
    position: relative;
    flex-direction:column;
    box-shadow: 0 0px 15px #AFAFAF;
    width: 280px;
    height: 280px;    
    border-radius: 15px;
    color: "white";
    margin-right: 25px;
`;
export const PrimeBox = styled(BoxMUI)`
position:absolute;
z-index: 2;
width: 75px;
height: 25px;
left:200px;
top: 15px;
size: 12px;
text-align: center ;
background-color: orange;
color: white;
border-radius: 20px;
text-shadow: 0px 2px 3px rgba(0, 0, 0, 0.5);
`;

export const Box = styled(BoxMUI)`
    display: 'flex';
    margin: '1px';
    height: 100;
    transform: 'scale(0.8)';
`;

export const TypographyTitle = styled(TypographyMUI)`
position:relative;

z-index: 1;
margin-top: 10px;
text-shadow: 0px 2px 3px rgba(0, 0, 0, 0.5);
`;

export const TypographyDescription = styled(TypographyMUI)`
position:relative;
width: 180px;
z-index: 1;
text-shadow: 0px 2px 3px rgba(0, 0, 0, 0.5);
`;
export const CardMedia = styled(CardMediaMUI)`
z-index: 0;
margin: 0;
width: 280px;
height: 200px;
border-top-left-radius: 15px;
border-top-right-radius: 15px;
`;
export const CardContent = styled(CardContentMUI)`
position: absolute;
display:flex;
flex-direction:column;
width: 280px;
height: 200px;
color:white;
`;
export const Grid = styled(GridMUI)`
display: grid;
margin: 0;
grid-template-columns: 100px 100px 80px;
position: relative;
height: 80px;
width: 280px;
font-family: 'Roboto';
align-items: center;
text-align: center;
`;
export const ContextGrid = styled(BoxMUI)`
font-size: 16px;
font-weight: bold;
`;
export const ItemGrid = styled(BoxMUI)`
font-style: normal;
font-size: 14px;
`;
export const BoxPlayGrid = styled(ButtonMUI)`
 position: absolute;
 z-index: 1;
 width: 60px;
 height: 60px;
 top: 170px;
 left: 205px;
 border: 4px solid #E5E8E8;
 border-radius: 100%;
 background-color: white;
`;
export const BoxPlayImg = styled(CardMediaMUI)`
position: absolute;
width: 15px;
height: 15px;
`;
export const PlayItem = styled.a` 
z-index: 2;
`;
