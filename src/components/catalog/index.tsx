import React, { FC } from 'react';

import {
    Card,
    ContextGrid,
    ItemGrid,
    TypographyDescription,
    TypographyTitle,
    CardMedia,
    CardContent,
    Grid,
    BoxPlayGrid,
    BoxPlayImg,
    PrimeBox,
} from './styles';

type BaseCardProps = {
  Icon: string;
  description: string;
  title: string;
  chanptersCount: string;
  itemCount: string;
  progressCard: string;
  linkToPage: string;
  primeContent: string;
};
import locked from '../../assets/images/catalog/locked.svg';
import play from '../../assets/images/catalog/play.svg';

const BasicCard: FC<BaseCardProps> = ({
    primeContent,
    Icon,
    description,
    title,
    chanptersCount,
    itemCount,
    linkToPage,
    progressCard,
}) => {
    return (
        <Card>
            {primeContent && <PrimeBox>Premium</PrimeBox>}
            <CardMedia
                component="img"
                src={Icon}
                alt="card media"></CardMedia>
            <CardContent>
                <TypographyDescription variant="h6">
                    {description}
                </TypographyDescription>
                <TypographyTitle variant="h4">{title}</TypographyTitle>
            </CardContent>
            <Grid container>
                <ContextGrid>{chanptersCount}</ContextGrid>
                <ContextGrid>{itemCount}</ContextGrid>
                <ContextGrid></ContextGrid>
                <ItemGrid>Chapter</ItemGrid>
                <ItemGrid>Items</ItemGrid>
                {primeContent ? (
                    <ItemGrid>Locked</ItemGrid>
                ) : (
                    <ItemGrid>{progressCard}%</ItemGrid>
                )}
            </Grid>
            <BoxPlayGrid color="inherit" variant="contained" href={linkToPage}>
                {primeContent ? (
                    <BoxPlayImg image={locked}></BoxPlayImg>
                ) : (
                    <BoxPlayImg image={play} color="green"></BoxPlayImg>
                )}
            </BoxPlayGrid>
        </Card>
    );
};
export default BasicCard;
