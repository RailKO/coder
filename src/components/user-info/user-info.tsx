import { Stack, Avatar, Typography } from '@mui/material';
import { Box } from '@mui/system';
import React, { FC } from 'react';
import { avatar } from '../../assets';
import { UserType } from '../../typings/types';
import { useTranslation } from 'react-i18next';

type PropsType = {
    user: UserType | null;
    info: string;
};

const UserInfo: FC<PropsType> = ({ user, info }) => {
    const { t } = useTranslation();

    return (
        <Box>
            <Stack direction="row" spacing={1}>
                <Box m={1}>
                    <Avatar src={user === null ? avatar : user.avatar} alt="user avatar" />
                </Box>
                <Box display="flex" alignItems="center">
                    <Typography variant="body2">{user === null ? t('anonymous_user') : user.name}</Typography>
                </Box>{' '}
                <Box display="flex" alignItems="center">
                    <Typography variant="body2" color="grey.400">
                        {info}
                    </Typography>
                </Box>
            </Stack>
        </Box>
    );
};

export default UserInfo;
