import { Box, Skeleton, Stack } from '@mui/material';
import React from 'react';

const SkeletonNewTopic = () => {
    return (
        <Box>
            <Stack spacing={0.1}>
                <Stack direction="row" spacing={0.1}>
                    <Skeleton variant="rectangular" width="80%" height={40} />
                    <Skeleton variant="rectangular" width="10%" height={40} />
                    <Skeleton variant="rectangular" width="10%" height={40} />
                </Stack>
                <Skeleton variant="rectangular" width="100%" height={40} />
                <Skeleton variant="rectangular" width="100%" height="70vh" />
            </Stack>
        </Box>
    );
};

export default SkeletonNewTopic;
