import React from 'react';
import { Box, Skeleton } from '@mui/material';

const SkeletonCatalogPopular = () => {
    return (
        <Box sx={{ width: 1140, height: 150 }}>
            <Skeleton variant="text" width={'1140px'} height={'150px'}></Skeleton>
        </Box>
    );
};

export default SkeletonCatalogPopular;
