import { Box, Skeleton } from '@mui/material';
import React from 'react';

const SkeletonCategories = () => {
    return (
        <Box mb={2}>
            <Skeleton variant="rectangular" width="100%" height={100} />
        </Box>
    );
};

export default SkeletonCategories;
