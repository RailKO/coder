import React from 'react';
import { Skeleton, Box } from '@mui/material';

const SkeletonCatalogTitle = () => {
    return (
        <Box sx={{ width: 1140, height: 150, marginTop: 3 }}>
            <Skeleton variant="text" width='110px' height='50px' />
            <Skeleton variant="rectangular" height={'100px'}></Skeleton>
        </Box>
    );
};

export default SkeletonCatalogTitle;
