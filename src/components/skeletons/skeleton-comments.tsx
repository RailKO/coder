import { Box, Skeleton, Stack } from '@mui/material';
import React from 'react';

const SkeletonComments = () => {
    return (
        <Box>
            <Stack spacing={0.1}>
                <Stack direction="row" spacing={0.1}>
                    <Skeleton variant="rectangular" width="70%" height={40} />
                    <Skeleton variant="rectangular" width="10%" height={40} />
                    <Skeleton variant="rectangular" width="10%" height={40} />
                    <Skeleton variant="rectangular" width="10%" height={40} />
                </Stack>
                <Stack spacing={0.1}>
                    <Skeleton variant="rectangular" width="100%" height={80} />
                    <Skeleton variant="rectangular" width="100%" height={60} />
                    <Skeleton variant="rectangular" width="100%" height={60} />
                    <Skeleton variant="rectangular" width="100%" height={60} />
                </Stack>
            </Stack>
        </Box>
    );
};

export default SkeletonComments;
