import { Box, Stack } from '@mui/material';
import React from 'react';
import SkeletonCategories from './skeleton-categories';
import SkeletonTags from './skeleton-tags';
import SkeletonTopicList from './skeleton-topic-list';

const SkeletonForum = () => {
    return (
        <Box m={1}>
            <SkeletonCategories />
            <Stack direction="row" spacing={1}>
                <Box flexShrink={1} flexGrow={1}>
                    <SkeletonTopicList />
                </Box>
                <Box flexShrink={0} flexGrow={0} width="230px">
                    <SkeletonTags />
                </Box>
            </Stack>
        </Box>
    );
};

export default SkeletonForum;
