import { Box, Skeleton, Stack } from '@mui/material';
import React from 'react';

const SkeletonViewTopic = () => {
    return (
        <Box>
            <Stack spacing={0.1}>
                <Stack direction="row" spacing={0.1}>
                    <Skeleton variant="rectangular" width="10%" height={40} />
                    <Skeleton variant="rectangular" width="90%" height={40} />
                </Stack>
                <Stack direction="row" spacing={0.1}>
                    <Skeleton variant="rectangular" width="10%" height="50vh" />
                    <Skeleton variant="rectangular" width="90%" height="50vh" />
                </Stack>
            </Stack>
        </Box>
    );
};

export default SkeletonViewTopic;
