import React from 'react';
import { Skeleton } from '@mui/material';

const SkeletonCatalogCard = () => {
    return (
        <Skeleton variant='text' width='1140px' height='400px'></Skeleton>


    );
};

export default SkeletonCatalogCard;
