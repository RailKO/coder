import { Box, Skeleton, Stack } from '@mui/material';
import React from 'react';

const SkeletonTopicList = () => {
    return (
        <Box>
            <Stack spacing={0.1}>
                <Skeleton variant="rectangular" width="100%" height={40} />
                <Skeleton variant="rectangular" width="100%" height={40} />
                <Skeleton variant="rectangular" width="100%" height="70vh" />
            </Stack>
        </Box>
    );
};

export default SkeletonTopicList;
