import { Box, Skeleton } from '@mui/material';
import React from 'react';

const SkeletonTags = () => {
    return (
        <Box>
            <Skeleton variant="rectangular" width="100%" height="50vh" />
        </Box>
    );
};

export default SkeletonTags;
