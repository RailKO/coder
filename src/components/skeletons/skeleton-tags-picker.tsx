import { Box, Skeleton } from '@mui/material';
import React from 'react';

const SkeletonTagsPicker = () => {
    return (
        <Box>
            <Skeleton variant="rectangular" width="100%" height={40} />
        </Box>
    );
};

export default SkeletonTagsPicker;
