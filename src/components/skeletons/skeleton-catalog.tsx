import React from 'react';
import { Box } from '@mui/material';
import { SkeletonCatalogCard, SkeletonCatalogPopular, SkeletonCatalogTitle } from './';

const SkeletonCatalog = () => {
    return (
        <Box display='flex' flexDirection='column' marginLeft='100px'>
            <SkeletonCatalogTitle></SkeletonCatalogTitle>
            <SkeletonCatalogPopular></SkeletonCatalogPopular>
            <SkeletonCatalogCard></SkeletonCatalogCard>
        </Box>
    );
};

export default SkeletonCatalog;
