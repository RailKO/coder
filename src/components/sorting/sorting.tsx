import { Box, Typography } from '@mui/material';
import React, { FC, useState } from 'react';
import { SortItemType } from 'src/typings/types';
import { ToggleButton, ToggleButtonGroup } from './sorting.styles';

type PropsType = {
    items: Array<SortItemType>;
    activeItem: SortItemType;
    title: string;
    handler: (id: number) => void;
};

const Sorting: FC<PropsType> = ({ items, activeItem, title, handler }) => {
    const [value, setValue] = useState(() => {
        return activeItem ? activeItem.title : '';
    });

    const handleChange = (event: React.MouseEvent<HTMLElement>, newValue: string) => {
        setValue(newValue);
        const item = items.find((item) => item.title === newValue);
        if (item) {
            handler(item.id);
        }
    };

    if (!items || items.length === 0) {
        return null;
    }

    return (
        <Box>
            <ToggleButtonGroup value={value} exclusive onChange={handleChange} aria-label={title} size="small">
                {items.map(({ id, title }) => (
                    <ToggleButton key={id} value={title} aria-label={title} size="small">
                        <Typography>{title}</Typography>
                    </ToggleButton>
                ))}
            </ToggleButtonGroup>
        </Box>
    );
};

export default Sorting;
