import { styled, ToggleButton as ToggleButtonMui, ToggleButtonGroup as ToggleButtonGroupMui } from '@mui/material';

export const ToggleButton = styled(ToggleButtonMui)`
  text-transform: none;
`;

export const ToggleButtonGroup = styled(ToggleButtonGroupMui)(({ theme }) => ({
    '& .MuiToggleButtonGroup-grouped': {
        'margin': theme.spacing(0.5),
        'border': 0,
        '&.Mui-disabled': {
            'border': 0,
        },
        '&:not(:first-of-type)': {
            'borderRadius': theme.shape.borderRadius,
        },
        '&:first-of-type': {
            'borderRadius': theme.shape.borderRadius,
        },
    },
}));
