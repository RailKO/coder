import React, { FC } from 'react';
import { createUrl } from '../../__data__/constants';
import { CategoryType } from '../../typings/types';
import { Box, NavLink, Paper, Typography } from './category.styles';

type PropsType = {
    category: CategoryType;
};

const Category: FC<PropsType> = ({ category }) => {
    const url = createUrl('link.coder.forum.group', { groupId: category.path });

    if (!url) {
        return null;
    }

    return (
        <Box height={100}>
            <NavLink to={url}>
                {({ isActive }) => (
                    <Paper elevation={0}>
                        <Typography
                            borderRadius={1}
                            align="center"
                            variant="h6"
                            color={isActive ? 'white' : 'grey.500'}
                            bgcolor={isActive ? 'secondary.main' : 'white'}
                        >
                            {category.title}
                        </Typography>
                    </Paper>
                )}
            </NavLink>
        </Box>
    );
};

export default Category;
