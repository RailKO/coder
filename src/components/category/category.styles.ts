import { Box as BoxMui, Paper as PaperMui, styled, Typography as TypographyMui } from '@mui/material';
import { NavLink as NavLinkRouter } from 'react-router-dom';

export const NavLink = styled(NavLinkRouter)`
  text-decoration: none;
`;

export const Box = styled(BoxMui)`
  flex-grow: 1;
  flex-basis: 0;
`;

export const Paper = styled(PaperMui)`
  height: 100%;
`;

export const Typography = styled(TypographyMui)`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  &:hover {
    color: black;
  }
`;
