
import React, { FC } from 'react';
import Alert from '@mui/material/Alert';
import { Link } from 'react-router-dom';

const UnloginInfo: FC = () => {
    return (
        <Link to ="https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-full-outer-join/">
            <Alert variant="outlined" severity="warning">
          Please <strong> login
                </strong> to see more details.
            </Alert>
        </Link>
    );
};

export default UnloginInfo;
