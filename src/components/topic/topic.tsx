import React, { FC } from 'react';
import { Avatar, Box, Stack, Typography } from '@mui/material';
import { avatar } from '../../assets';
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { CategoryTopicType, CategoryType } from '../../typings/types';
import { createUrl } from '../../__data__/constants';
import { Link } from './topic.styles';
import { changeToKilo, unixToDate } from '../../utils/utils';
import { useTranslation } from 'react-i18next';

type PropsType = {
    topic: CategoryTopicType;
    currentCategory: CategoryType;
};
// FIXME переделать minWidth="80px" для контейнера просмотров и голосов
const Topic: FC<PropsType> = ({ topic, currentCategory }) => {
    const { t } = useTranslation();

    const url = createUrl('link.coder.forum.topic', { groupId: currentCategory.path, topicId: topic.id.toString() });

    const creationDate = unixToDate(topic.creationDate);

    return (
        <Box mb={1} p={1} borderBottom={1} borderColor="grey.300">
            <Stack direction="row" justifyContent="space-between">
                <Stack direction="row">
                    <Box mr={2}>
                        <Avatar src={topic.author === null ? avatar : topic.author.avatar} alt="user avatar" />
                    </Box>
                    <Box>
                        <Link to={url}>
                            <Typography variant="subtitle1" color="black">
                                {topic.title}
                            </Typography>
                        </Link>
                        <Typography variant="body2" color="grey.400">
                            {`${topic.author === null ? t('anonymous') : topic.author.name} 
                            ${t('created_at')} ${creationDate}`}
                        </Typography>
                    </Box>
                </Stack>
                <Stack direction="row" spacing={2} alignItems="center">
                    <Box display="flex" minWidth="80px">
                        <ThumbUpAltIcon color="disabled" />
                        <Typography ml={1}>{changeToKilo(topic.voteCount)}</Typography>
                    </Box>
                    <Box display="flex" minWidth="80px">
                        <VisibilityIcon color="disabled" />
                        <Typography ml={1}>{changeToKilo(topic.viewCount)}</Typography>
                    </Box>
                </Stack>
            </Stack>
        </Box>
    );
};

export default Topic;
