import { styled } from '@mui/material/styles';
import { Link as LinkRouter } from 'react-router-dom';

export const Link = styled(LinkRouter)`
  text-decoration: none;
`;
