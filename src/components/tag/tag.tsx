import { Box, Chip } from '@mui/material';
import React, { FC } from 'react';
import { TagType } from 'src/typings/types';

type PropsType = {
  tag: TagType;
};

const Tag: FC<PropsType> = ({ tag }) => {
    return (
        <Box m={0.5}>
            <Chip label={`${tag.name} | ${tag.numTopics}`} onClick={() => {}} />
        </Box>
    );
};

export default Tag;
