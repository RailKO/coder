import { Alert, AlertTitle, Box, Button } from '@mui/material';
import React, { FC } from 'react';

type PropsType = {
    message: string;
};

const Banner: FC<PropsType> = ({ message }) => {
    return (
        <Box mb={2}>
            <Alert
                severity="error"
                action={
                    <Button size="small" color="inherit" onClick={() => window.location.reload()} title="banner button">
                        Reload
                    </Button>
                }
            >
                <AlertTitle>Error</AlertTitle>
                {message}
            </Alert>
        </Box>
    );
};

export default Banner;
