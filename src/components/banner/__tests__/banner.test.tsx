import React from 'react';
import { render } from '@testing-library/react';
import { describe, it, expect } from '@jest/globals';
import Banner from '../banner';

describe('banner test suite', () => {
    it('banner render', () => {
        const { container } = render(<Banner handler={() => {}} message="test" />);
        expect(container).toMatchSnapshot();
    });
});
