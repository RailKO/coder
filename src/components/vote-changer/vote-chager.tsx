import { Box, Button, Typography } from '@mui/material';
import React, { FC, useState } from 'react';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { changeToKilo } from '../../utils/utils';

type PropsType = {
    voteCount: number;
    onVoteChange: (value: number) => void;
};

// TODO переписать, когда появится норм авторизация
const VoteChanger: FC<PropsType> = ({ voteCount, onVoteChange }) => {
    const [value, setValue] = useState(voteCount);

    const upVoteHandler = () => {
        setValue(value + 1);
        onVoteChange(value);
    };

    const downVoteHandler = () => {
        setValue(value === 0 ? 0 : value - 1);
        onVoteChange(value);
    };

    return (
        <Box p={1}>
            <Button variant="contained" aria-label="up vote" onClick={upVoteHandler} color="secondary">
                <ArrowDropUpIcon />
            </Button>
            <Typography mt={1} mb={1} align="center" color="grey.500">
                {changeToKilo(value)}
            </Typography>
            <Button variant="contained" aria-label="down vote" onClick={downVoteHandler} color="secondary">
                <ArrowDropDownIcon />
            </Button>
        </Box>
    );
};

export default VoteChanger;
