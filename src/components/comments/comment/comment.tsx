import { Box, Typography } from '@mui/material';
import React, { FC } from 'react';
import UserInfo from '../../../components/user-info/user-info';
import { CommentType } from '../../../typings/types';
import { unixToDate } from '../../../utils/utils';

type PropsType = {
    comment: CommentType;
};

const Comment: FC<PropsType> = ({ comment }) => {
    const getUserInfo = () => {
        return comment.updationDate === comment.creationDate
            ? unixToDate(comment.creationDate)
            : `Last Edit: ${unixToDate(comment.updationDate)}`;
    };

    return (
        <Box>
            <UserInfo user={comment.author} info={getUserInfo()} />
            <Typography variant="body1" ml={8}>
                {comment.content}
            </Typography>
        </Box>
    );
};

export default Comment;
