import { Button as ButtonMui, styled } from '@mui/material';

export const Button = styled(ButtonMui)`
  text-transform: none;
`;
