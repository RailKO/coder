import { Stack, TextField } from '@mui/material';
import { Box } from '@mui/system';
import React, { FC, useState } from 'react';
import { Button } from './comment-editor.styles';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { CommentToInsertType } from '../../../typings/types';
import { useInsertForumCommentMutation } from '../../../__data__/services/api';

type PropsType = {
    onPostComment: () => void;
};

const CommentEditor: FC<PropsType> = ({ onPostComment }) => {
    const [insertComment] = useInsertForumCommentMutation();
    const [content, setContent] = useState('');
    const { t } = useTranslation();
    const params = useParams();

    const onChangeContent = ({ target }) => {
        setContent(target.value);
    };

    const onPostButtonHandler = () => {
        const comment: CommentToInsertType = {
            topicId: Number(params.topicId),
            content,
            authorId: 1,
        };
        insertComment(comment);
        onPostComment();
        setContent('');
    };

    return (
        <Box p={1}>
            <Stack justifyContent="center" spacing={1} alignItems="flex-end">
                <TextField
                    multiline
                    fullWidth
                    color="secondary"
                    maxRows={10}
                    minRows={4}
                    placeholder={t('type_comment_here')}
                    value={content}
                    onChange={onChangeContent}
                />
                <Button variant="contained" color="secondary" onClick={onPostButtonHandler}>
                    {t('send')}
                </Button>
            </Stack>
        </Box>
    );
};

export default CommentEditor;
