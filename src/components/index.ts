export { default as Comment } from './comments/comment/comment';
export { default as CommentEditor } from './comments/comment-editor/comment-editor';
export { default as Search } from './search/search';
export { default as Sorting } from './sorting/sorting';
export { default as Tag } from './tag/tag';
export { default as UserInfo } from './user-info/user-info';
export { default as VoteChanger } from './vote-changer/vote-chager';
export { default as Category } from './category/category';
export { default as Topic } from './topic/topic';
// делать прямым импортом или замокать лотти
export { default as ErrorBoundary } from './error-boundary/error-boundary';
export { default as Banner } from './banner/banner';
