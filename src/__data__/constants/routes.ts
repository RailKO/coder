import { getNavigations } from '@ijl/cli';

const nav = getNavigations();

type StringRecord = Record<string, string>;

export const createUrl = (id, params: StringRecord) => {
    const url = nav[id];

    if (!url) {
        return '';
    }

    if (!params) {
        return url;
    }

    return Object.entries(params).reduce((acc, [key, value]) => acc.replace(`:${key}`, value), url);
};

export const URLS = {
    catalog: nav['link.coder.topics'],
    forum: nav['link.coder.forum'],
    forumGroup: nav['link.coder.forum.group'],
    forumTopic: nav['link.coder.forum.topic'],
    forumNewTopic: nav['link.coder.forum.new.topic'],
    // Страница с топиками
    topics: nav['link.coder.topics'],
    // Сам топик
    topic: nav['link.coder.topic'],
    topicSection: nav['link.coder.topic.section'],
    auth: {
        urlSignIn: nav['link.coder.auth.sign-in'],
        urlSignUp: nav['link.coder.auth.sign-up'],
        urlResetPassw: nav['link.coder.auth.reset-password'],
        urlResetPasswMessage: nav['link.coder.auth.reset-password-message'],
        urlResetPasswMessageLogged: nav['link.coder.auth.reset-password-message-logged'],
    },
    base: nav['coder.main'],
};
