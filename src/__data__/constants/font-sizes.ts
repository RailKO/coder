export const SMALL_FONT_SIZE = '12px';
export const MIDDLE_FONT_SIZE = '14px';
export const LARGE_FONT_SIZE = '18px';
