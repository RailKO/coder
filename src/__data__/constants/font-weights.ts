export const LIGHT_FONT_WEIGHT = '200';
export const MEDIUM_FONT_WEIGHT = '600';
export const HEAVY_FONT_WEIGHT = '800';
