export const FORUM_TOPIC_CATEGOREIS_ENDPOINT = '/forum/topic-categories';
export const FORUM_TOPIC_COMMENTS_ENDPOINT = '/forum/topic-comments';
export const FORUM_TOPIC_LIST_ENDPOINT = '/forum/topic-list';
export const FORUM_TOPIC_TAGS_ENDPOINT = '/forum/topic-tags';
export const FORUM_TOPIC_ENDPOINT = '/forum/topic';
export const CATALOG_ENDPOINT = '/catalog';
export const GET_LEARN_TOPIC = '/gettopic';
export const AUTH_SIGN_UP = '/auth/sign-up';

