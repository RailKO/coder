export const GREY_COLOR = '#9E9E9E';
export const WHITE_COLOR = '#FFFFFF';
export const BLACK_COLOR = '#000000';
export const BLACK_LIGHT_COLOR = '#606060';
export const GREY_LIGHT_COLOR = '#F5F5F5';
export const BUTTON_ACTIVE_COLOR = '#829898';
