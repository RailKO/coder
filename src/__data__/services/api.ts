import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { getConfigValue } from '@ijl/cli';
import {
    FORUM_TOPIC_CATEGOREIS_ENDPOINT,
    FORUM_TOPIC_COMMENTS_ENDPOINT,
    FORUM_TOPIC_ENDPOINT,
    FORUM_TOPIC_LIST_ENDPOINT,
    FORUM_TOPIC_TAGS_ENDPOINT,
    CATALOG_ENDPOINT,
    GET_LEARN_TOPIC,
    AUTH_SIGN_UP,
} from '../constants';


export const api = createApi({
    reducerPath: 'coder',
    baseQuery: fetchBaseQuery({
        baseUrl: getConfigValue('coder.api'),
        // fetchFn: fetch,
    }),
    endpoints: (builder) => ({
        getForumTopicTags: builder.query({
            query: () => FORUM_TOPIC_TAGS_ENDPOINT,
        }),

        getForumTopic: builder.query({
            query: (id) => `${FORUM_TOPIC_ENDPOINT}/${id}`,
        }),

        getForumTopicList: builder.query({
            query: ({ categoryId, page = 1 }) => `${FORUM_TOPIC_LIST_ENDPOINT}/${categoryId}?page=${page}`,
        }),

        getForumTopicComments: builder.query({
            query: ({ topicId, page = 1 }) => `${FORUM_TOPIC_COMMENTS_ENDPOINT}/${topicId}?page=${page}`,
        }),

        getForumTopicCategories: builder.query({
            query: () => FORUM_TOPIC_CATEGOREIS_ENDPOINT,
        }),

        getForumTopicCategoryByPath: builder.query({
            query: (groupId) => `${FORUM_TOPIC_CATEGOREIS_ENDPOINT}/${groupId}`,
        }),

        findForumTags: builder.mutation({
            query: (name) => ({
                url: `${FORUM_TOPIC_TAGS_ENDPOINT}?search=${name}`,
                method: 'GET',
            }),
        }),

        insertForumTag: builder.mutation({
            query: (name) => ({
                url: FORUM_TOPIC_TAGS_ENDPOINT,
                method: 'POST',
                body: { name },
            }),
        }),

        insertForumComment: builder.mutation({
            query: (comment) => ({
                url: FORUM_TOPIC_COMMENTS_ENDPOINT,
                method: 'POST',
                body: comment,
            }),
        }),

        insertForumTopic: builder.mutation({
            query: (topic) => ({
                url: FORUM_TOPIC_ENDPOINT,
                method: 'POST',
                body: topic,
            }),
        }),

        getAuth: builder.query({
            query: () => AUTH_SIGN_UP,
        }),

        getCatalog: builder.query({
            query: () => CATALOG_ENDPOINT,
        }),


        getLearnTopic: builder.query({
            query: (topic) => `${GET_LEARN_TOPIC}/${topic}`,
        }),

        getLearnTopicSection: builder.query({
            query: ({ topic, section = 'overview' }) => `${GET_LEARN_TOPIC}/${topic}/${section}`,
        }),
    }),
});

export const {
    useGetForumTopicTagsQuery,
    useGetForumTopicQuery,
    useGetForumTopicCategoriesQuery,
    useGetForumTopicCategoryByPathQuery,
    useGetForumTopicCommentsQuery,
    useGetForumTopicListQuery,
    useFindForumTagsMutation,
    useInsertForumTagMutation,
    useInsertForumCommentMutation,
    useInsertForumTopicMutation,
    useGetCatalogQuery,
    useGetLearnTopicQuery,
    useGetLearnTopicSectionQuery,
    useGetAuthQuery,
} = api;
