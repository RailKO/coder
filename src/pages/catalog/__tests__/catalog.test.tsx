import React from 'react';
import { rest } from 'msw';
import { describe, it, expect, jest, afterAll, beforeAll } from '@jest/globals';
import { setupServer } from 'msw/node';
import { render, waitFor, screen } from '@testing-library/react';
import 'whatwg-fetch';


import CatalogComponent from '../';
import { ApiProvider } from '@reduxjs/toolkit/dist/query/react';
import { BrowserRouter } from 'react-router-dom';
import { createTheme } from '@mui/material';
import { api } from '../../../__data__/services/api';


export const theme = createTheme({
    palette: {
        mode: 'light',
        primary: {
            main: '#ffffff',
        },
        secondary: {
            main: '#465a65',
        },
    },
});

jest.mock('@ijl/cli', () => {
    return {
        getConfigValue: () => '/api',
        getNavigations: () => ({ 'coder.main': '/coder' }),
    };
});

const server = setupServer(
    rest.get('/api/catalog', (req, res, ctx) => {
        return res(
            ctx.json(require('../../../../stubs/api/catalog/cardData.json')),
        );
    }),
);

describe('Test catalog', () => {
    beforeAll(() => {
        server.listen();
    });

    afterAll(()=>{
        server.close();
    });

    it('check render Catalog', async () => {
        // eslint-disable-next-line max-len
        const { container } = render(<BrowserRouter><ApiProvider api={api}><CatalogComponent /></ApiProvider></BrowserRouter>);
        expect(container).toMatchSnapshot();

        await waitFor(()=> screen.getAllByText('Featured'));
        expect(container).toMatchSnapshot();
    });
});

