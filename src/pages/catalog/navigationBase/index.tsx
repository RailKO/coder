import React, { FC } from 'react';

import { GlobalWrap,
    ToolSetWrapper,
    PageLabel,
    PageTitle,
    HistoryIcon,
    FavoritIcon, LeftFlex, RightFlex, HistoryButton, FavoritButton,
} from './styles';

import favorit from '../../../assets/images/catalog/star.svg';
import history from '../../../assets/images/catalog/history.svg';

const label = 'LeetCode Explore';
const title = 'Welcome to';

const NavigationBaseComponent: FC = () =>{
    return (
        <GlobalWrap>
            <LeftFlex>
                <PageLabel>{title}</PageLabel>
                <PageTitle>{label}</PageTitle>
            </LeftFlex>
            <RightFlex>
                <ToolSetWrapper>
                    <FavoritButton href="">
                        <HistoryIcon src={favorit}></HistoryIcon>
                    </FavoritButton>
                    <HistoryButton href="">
                        <FavoritIcon src={history}></FavoritIcon>
                    </HistoryButton>
                </ToolSetWrapper>
            </RightFlex>
        </GlobalWrap>
    );
};
export default NavigationBaseComponent;
