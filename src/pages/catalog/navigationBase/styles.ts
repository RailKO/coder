import styled from '@emotion/styled';

export const GlobalWrap = styled.div`
position: absolute;
display: flex;
align-items:stretch;
width: 1150px;
height: 100px;
left: 100px;
top: 40px;
color: #AFAFAF;
`;
export const LeftFlex = styled.div`
display: flex;
align-items:stretch;
width: 1000px;
height: 100px;
top: 0px;
color:#1F1F1F;
`;
export const RightFlex = styled.div`
display: flex;
width: 1000px;
height: 100px;
align-items:flex-end;
color:#000000;
`;
export const PageLabel = styled.div`
width: 1000px;
height: 40px;
left: 0px;
top: 0px;
font-family: 'Roboto';
font-style: normal;
font-weight: 700;
font-size: 18px;
line-height: 21px;
align-items:flex-Start;
color: #A0A0A9;
`;

export const PageTitle = styled.div`
position: absolute;
display:flex;
width: 1000px;
height: 45px;
left: 0px;
top: 50px;
font-family: 'Roboto';
font-style: normal;
font-weight: 700;
font-size: 24px;
line-height: 28px;
align-items:flex-end;
color: #6B6B6B;
`;

export const ToolSetWrapper = styled.div`
display: flex;
width: 100px;
height: 50px;
align-items:flex-end;
left: 0px;
top: 49px;
`;
export const HistoryButton = styled.a`
width: 25px;
height: 25px;
margin: 15px 5px; 
`;
export const HistoryWrapper = styled.div`
width: 50px;
height: 50px;
`;

export const HistoryIcon = styled.img`
  width: 30px;
  height: 30px;
  margin: 15px 5px; 
`;

export const FavoritButton = styled.a`
width: 25px;
height: 25px;
margin: 15px 25px;
`;
export const FavoritWrapper = styled.div`
width: 50px;
height: 50px;

`;
export const FavoritIcon = styled.img`
  width: 30px;
  height: 30px;
  margin: 15px 5px;  
`;

