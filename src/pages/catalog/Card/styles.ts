import styled from '@emotion/styled';
import GridMUI from '@mui/material/Grid';
// основа
export const Layout = styled.div`
position: absolute;
display: flex;
flex-direction:column;
width: auto;
height: 405px;
left: 100px;
top: 240px;
color:#6B6B6B;
border-radius: 30px;
`;

export const GridLayout = styled(GridMUI)`
position: absolute;
display: flex;
flex-direction:column;
width: auto;
height: 350px;
left: 100px;
top: 240px;
color:#6B6B6B;
border-radius: 30px;
`;

// верхняя часть
export const UpFlex = styled.div`
display: flex;
width: auto;
height: auto;
`;
export const Title = styled.div`
display: flex;
width: 1000px;
font-family: 'Roboto';
font-size: 22px;
color: #6B6B6B;
margin-top: 15px;
`;

export const ContentText = styled.div`
display: flex;
width:auto;
height:auto;
margin: 5px 15px;
font-family: 'Roboto';

color: #000000;
`;

export const MoreButton = styled.a`
width: 70px;
height: 30px;

margin: 20px 30px;
left: calc(50% - 80px/2 + 471.5px);
top: calc(50% - 30px/2 - 81px);
background: #FFFFFF;
box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.25);
border-radius: 25px;
`;
// нижняя часть
export const DownFlex = styled.div`
display: flex;
align-items:flex-Start;
width: auto;
height: auto;
`;
