import React, { FC } from 'react';
import {
    DownFlex,
    UpFlex,
    Title,
    MoreButton,
    ContentText,
    GridLayout,
} from './styles';

import BasicCard from '../../../components/catalog/index';
import { useGetCatalogQuery } from '../../../__data__/services/api';
import loadImg from './imageLoader';
import { SkeletonCatalog } from '../../../components/skeletons/index';

const BaseComponent: FC = () =>{
    const { data, isLoading } = useGetCatalogQuery(undefined);
    if (isLoading || (!isLoading && !data)) {
        return <SkeletonCatalog/>;
    }
    return (
        <GridLayout>
            {data?.dataFeatured?.map((element, index) => (<div key={index}>
                <UpFlex>
                    <Title>{element.title}</Title>
                    <MoreButton href="">
                        <ContentText>More</ContentText>
                    </MoreButton>
                </UpFlex>
                <DownFlex>
                    { element.items?.map((item, idx) => (
                        <BasicCard key={idx}
                            Icon={loadImg(item.iconCard)}
                            description={item.description}
                            title={item.title}
                            chanptersCount={item.chanptersCount}
                            itemCount={item.itemCount}
                            linkToPage={item.linkToPage}
                            progressCard={item.progressCard}
                            primeContent={item.primeContent}
                        >
                        </BasicCard>))
                    }
                </DownFlex>
            </div>))
            }
        </GridLayout>
    );
};
export default BaseComponent;
