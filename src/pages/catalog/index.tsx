import React, { FC } from 'react';
import NavigationBaseComponent from './navigationBase/index';
import BaseComponent from './Card/index';
import PopularComponent from './popularBase/index';


import { ExplorerBase } from './styles';


const componentCatalog: FC = () => {
    return (
        <ExplorerBase>
            <NavigationBaseComponent></NavigationBaseComponent>
            <PopularComponent></PopularComponent>
            <BaseComponent></BaseComponent>
        </ExplorerBase>

    );
};

export default componentCatalog;
