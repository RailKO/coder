import { Box } from '@mui/system';
import Lottie from 'lottie-react';
import React, { FC } from 'react';
import { notFoundAnimation } from '../../assets/lotty-files';

const NotFoundPage: FC = () => {
    return (
        <Box>
            <Lottie animationData={notFoundAnimation} />
        </Box>
    );
};

export default NotFoundPage;
