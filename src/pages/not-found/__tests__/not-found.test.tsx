import React from 'react';
import { describe, expect, it } from '@jest/globals';
import 'whatwg-fetch';
import NotFoundPage from '../not-found';
import { render } from '../../../utils/test-utils';

// eslint-disable-next-line react/display-name
jest.mock('lottie-react', () => () => <div>Lottie is here</div>);

describe('Test NotFound page', () => {
    it('check render', async () => {
        const { container } = render(<NotFoundPage />);
        expect(container).toMatchSnapshot();
    });
});
