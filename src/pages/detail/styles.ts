import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
export const ContentWrapper = styled(Box)`
    box-sizing: border-box;
    height: 100%;
    width: 100%;
`;
