/**
 * @jest-environment jsdom
 * @jest-environment-options {"url": "http://localhost:8099/coder/topics/dynamic-programming"}
 */
import React from "react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import {
  describe,
  expect,
  it,
  beforeAll,
  afterEach,
  afterAll,
} from "@jest/globals";
import { waitFor, screen, fireEvent } from "@testing-library/react";
import "whatwg-fetch";
import { render } from "../../../utils/test-utils";
import AsideComponent from "../";

jest.mock(`lottie-react`, () => () => <div>111</div>);

const server = setupServer(
  rest.get("/api/gettopic/dynamic-programming", (req, res, ctx) => {
    return res(
      ctx.json({
        status: "success",
        title: "Dynamic Programming",
        slug: "dynamic-programming",
        overview:
          "In this explore card, we're going to go over the basics of DP, provide you with a framework for solving DP problems, learn about common patterns, and walk through many examples.",
        chapters: [
          {
            title: "Dynamic Programming",
            section: "dynamic-programming",
            isLock: false,
            content: "<p>Динамическое программирование</p>",
          },
          {
            title: "Arrays and Strings",
            section: "arrays-and-strings",
            isLock: false,
            content: "<p>Массивы и строки</p>",
          },
          {
            title: "Linked list",
            section: "linked-list",
            isLock: true,
            content: "<p>Ссылочный список</p>",
          },
        ],
      })
    );
  }),

  rest.get("/api/gettopic/dynamic-programming/overview", (req, res, ctx) => {
    return res(
      ctx.json({
        title: "Overview",
        slug: "overview",
        content: [
          { data: "## Overview" },
          {
            data: "In this explore card, we're going to go over the basics of DP, provide you with a framework for solving DP problems, learn about common patterns, and walk through many examples.No prior knowledge of DP is required for this card, but you will need a solid understanding of recursion, a basic understanding of what greedy algorithms are, and general knowledge such as big O and hash tables. If you aren't familiar with recursion yet, check out the recursion explore card first.",
          },
        ],
      })
    );
  })
);

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: () => ({ topic: "dynamic-programming" }),
}));

describe("AsideComponent", () => {
  beforeAll(() => {
    server.listen();
  });

  afterEach(() => {
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });
  it("testRender", async () => {
    const { container } = render(<AsideComponent />);
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText("Dynamic Programming"));
    expect(container).toMatchSnapshot();
  });
});
