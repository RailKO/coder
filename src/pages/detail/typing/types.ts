export type TotalData = {
    status: string;
    title: string;
    chapters: Array<CategoryType>;
};

export type CategoryType = {
    title: string;
    description: string;
};
