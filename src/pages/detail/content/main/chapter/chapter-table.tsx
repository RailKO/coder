import React, { FC } from 'react';
import { EnabledComponent, Discussion, Assign, LockComponent } from '../../../../../assets/icons';
import { ListItemButtonFix, StyledList } from './styles';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { useNavigate, useParams } from 'react-router-dom';
import { ChaptersType } from 'src/typings/detail-typings';

const ChapterTable: FC<ChaptersType> = ({ chapters, overview }) => {
    const navigate = useNavigate();

    const { topic } = useParams();

    console.log('сравнение', location.pathname == '/coder/topics/' + topic + '/overview');

    // const secondary = "In this explore card, we're going to go over the basics of DP, provide you with a
    // framework for solving DP problems, learn about common patterns, and walk through many examples.";
    return (
        <StyledList>
            <ListItemButtonFix
                alignItems="flex-start"
                active={location.pathname == '/coder/topics/' + topic + '/overview'}
                onClick={async (event) => {
                    const url = '/coder/topics/' + topic + '/overview';
                    navigate(url);
                }}
            >
                <ListItemIcon>
                    <Assign />
                </ListItemIcon>
                <ListItemText primary="Overview" secondary={overview} />
            </ListItemButtonFix>
            {chapters.map((item, idx) => {
                return (
                    <ListItemButtonFix
                        key={idx}
                        active={location.pathname == '/coder/topics/' + topic + '/' + item.section}
                        onClick={async (event) => {
                            if (!item.isLock) {
                                const url = '/coder/topics/' + topic + '/' + item.section;
                                navigate(url);
                            }
                        }}
                    >
                        <ListItemIcon>{item.isLock ? <LockComponent /> : <EnabledComponent />}</ListItemIcon>
                        <ListItemText primary={item.title} />
                    </ListItemButtonFix>
                );
            })}
            <ListItemButton>
                <ListItemIcon>
                    <Discussion />
                </ListItemIcon>
                <ListItemText primary="Discuss" secondary="19 topics" />
            </ListItemButton>
        </StyledList>
    );
};

export default ChapterTable;
