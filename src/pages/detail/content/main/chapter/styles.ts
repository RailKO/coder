import { styled } from '@mui/material';
import ListItemButton from '@mui/material/ListItemButton';
import List from '@mui/material/List';

type PropsType = {
    active: boolean;
};

export const ListItemButtonFix = styled(ListItemButton)<PropsType>`
    ${(props) => {
        if (props.active) {
            return `
            color: rgba(255,255,255,0.8); 
            background: #222222; 
            &:hover {
                color: #000;
            }
            .css-83ijpv-MuiTypography-root {
                color: inherit;  
            }
            .css-i4bv87-MuiSvgIcon-root {
                color: white;
            }
            `;
        }
    }}
`;

export const StyledList = styled(List)`
    border-radius: 15px;
    box-shadow: inset 0 4px 7px 1px #ffffff, inset 0 -5px 20px rgb(173 186 204 / 25%), 0 2px 6px rgb(0 21 64 / 14%), 0 10px 20px rgb(0 21 64 / 5%);
    border-color: #222222;
    overflow: hidden;
    padding: 0;
    margin-top: -30px;
    z-index: 1;
    background-color: #fff;
    .css-83ijpv-MuiTypography-root {
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
    }
    /* 
    .css-1u1tvf2-MuiButtonBase-root-MuiListItemButton-root.active {
        background: #222222;
        color: #fff;
        .css-10hburv-MuiTypography-root{
            color: #fff;
        }
        .css-83ijpv-MuiTypography-root {
            color: #fff;
            opacity: 0.5;
        }
        svg {
            fill: #fff;
        }
        &:hover{
            background: #000;
        }
    } */
`;

export const StyledHeaderItem = styled(ListItemButton)`
    /* display: -webkit-box; 
    -webkit-line-clamp: 2; 
    -webkit-box-orient: vertical; 
    overflow: hidden; */
    /* max-height: 200px; */
    /* white-space: nowrap; Запрещаем перенос строк */
    /* overflow: hidden; Обрезаем все, что не помещается в область */
    /* background: #222222;
    .css-10hburv-MuiTypography-root{
        color: #fff;
    }
    .css-83ijpv-MuiTypography-root {
        color: #fff;
        opacity: 0.5;
    }
    svg {
        fill: #fff;
    }
    &:hover{
        background: #000;
    }
    .active {
        background: red;
    } */
`;
