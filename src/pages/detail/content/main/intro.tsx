import React, { FC } from 'react';
import Typography from '@mui/material/Typography';
const IntroParagraph: FC = () => {
    return (
        <>
            <Typography variant="h3" component="h3" gutterBottom>
        Introduction
            </Typography>
            <Typography variant="body1" component="p" gutterBottom>
                In this explore card, we are going to go over the basics of <strong>DP</strong>,
                provide you with a framework for solving DP problems, learn about common patterns, and walk through many examples.
            </Typography>
        </>
    );
};

export default IntroParagraph;
