import { styled as styledMu } from '@mui/material';
import ListItemButton from '@mui/material/ListItemButton';
import List from '@mui/material/List';

export const StyledContentList = styledMu(List)`
    border-radius: 15px;
    border: 1px solid #dddddd;
    overflow: hidden;
    padding: 0;
`;

export const StyledHeaderItem = styledMu(ListItemButton)`
    background: white;
    /* .css-10hburv-MuiTypography-root {
        color:blue;
    } */
    /* .css-10hburv-MuiTypography-root{
        color: white;
    }
    .css-83ijpv-MuiTypography-root {
        color: white;
        opacity: 0.5;
    } */
    /* svg {
        fill: white;
    } */
    /* &:hover{
        background: #000;
    } */
`;
