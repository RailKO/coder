import React, { FC } from 'react';
import TableThemes from './table';
import ChapterTable from './chapter/chapter-table';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import { useParams } from 'react-router-dom';
import { CategoryTopicListType } from '../../../../typings/detail-typings';
import { useGetLearnTopicSectionQuery } from '../../../../__data__/services/api';
import UnloginInfo from '../../../../components/detail-login/unlogin-info';
import MuiMarkdown from 'mui-markdown';

const MainContent: FC<CategoryTopicListType> = ({ overview, contentChapters }) => {
    const { topic, section } = useParams();

    const { sectionData, isLoadingSection } = useGetLearnTopicSectionQuery(
        { topic, section },
        {
            selectFromResult: (result) => ({
                isLoadingSection: result.isLoading,
                sectionData: result.data,
                ...result,
            }),
        },
    );

    if (isLoadingSection && !sectionData) {
        return <div>Loading...</div>;
    }

    return (
        <Grid container spacing={2}>
            <Grid item xs={4}>
                <ChapterTable chapters={contentChapters} overview={overview} />
            </Grid>
            <Grid item xs={8}>
                <Stack spacing={2}>
                    {sectionData?.content.map((item, idx) => {
                        const s = '' + item.data;
                        return <MuiMarkdown key={idx}>{s}</MuiMarkdown>;
                    })}
                    {/* <IntroParagraph/> */}
                    {section !== 'overview' ? (
                        ''
                    ) : (
                        <div>
                            <UnloginInfo />
                            <TableThemes title="Intro to dynamic programming" />
                        </div>
                    )}
                </Stack>
            </Grid>
        </Grid>
    );
};

export default MainContent;
