import React, { FC } from 'react';
import { StyledContentList, StyledHeaderItem } from './styles';
import Stack from '@mui/material/Stack';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { EnabledComponent, LockComponent } from '../../../../assets/icons';

import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Skeleton from '@mui/material/Skeleton';

const TableThemes: FC = (props: any) => {
    return (
        <Stack spacing={2}>

            <StyledContentList>
                {/* Header */}
                <StyledHeaderItem>
                    <ListItemText primary={props.title}/>
                    <ListItemIcon>
                        <EnabledComponent/>
                    </ListItemIcon>
                </StyledHeaderItem>

                {/* Content */}
                <ListItemButton>
                    <ListItemIcon>
                        <FormControlLabel control={<Checkbox defaultChecked={false} disabled />} label="A" />
                    </ListItemIcon>
                    <Skeleton variant="text" width={210} />
                </ListItemButton>
                <ListItemButton>
                    <ListItemIcon>
                        <FormControlLabel control={<Checkbox defaultChecked={false} disabled />} label="A" />
                    </ListItemIcon>
                    <Skeleton variant="text" width={210} />
                </ListItemButton>
            </StyledContentList>


            <StyledContentList>
                <StyledHeaderItem>
                    <ListItemText primary={props.title}/>
                    <ListItemIcon>
                        <LockComponent/>
                    </ListItemIcon>
                </StyledHeaderItem>
            </StyledContentList>

        </Stack>
    );
};

export default TableThemes;
