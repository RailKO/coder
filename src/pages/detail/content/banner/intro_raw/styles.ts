import { styled } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

export const StyledTextBox = styled(Box)`
    color: white;
    text-shadow: 0 4px 20px rgb(0 0 0 / 30%);
    text-align: center;
    padding-top: 150px;
`;

export const FavButtonStyle = styled(Button)`
    color: white;
    background-color: #fbc12a;
    &:hover {
        background-color: #fbc12a;
    }
`;
