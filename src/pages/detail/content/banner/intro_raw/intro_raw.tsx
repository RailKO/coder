import React, { FC } from 'react';
import { StyledTextBox } from './styles';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Favorite, ArrowLeft } from '../../../../../assets/icons';
import { groupNavigationBase } from '../banner';

const IntroRaw: FC<groupNavigationBase> = ({ title }) => {
    return (
        <Grid container spacing={2}>
            <Grid item xs={3} textAlign="center">
                <Button variant="text" href="/" startIcon={<ArrowLeft />}>Back to Explore</Button>
                {/* <Button variant="text" href="/coder" startIcon={<ArrowLeft />}>Back to Explore</Button> */}
            </Grid>
            <Grid item xs={6}>
                <StyledTextBox>
                    <Typography variant="subtitle1" component="h6" gutterBottom align="center">
                    Detailed Explanation of
                    </Typography>
                    <Typography variant="h4" gutterBottom align="center">
                        {title}
                    </Typography>
                </StyledTextBox>
            </Grid>
            <Grid item xs={3} textAlign="center">
                <Button
                    color="warning"
                    variant="contained" endIcon={<Favorite />}>
                    favorite
                </Button>
            </Grid>
        </Grid>
    );
};

export default IntroRaw;
