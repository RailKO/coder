import React, { FC } from 'react';
import { BaseBanner } from './styles';
import IntroRaw from './intro_raw/intro_raw';

export type groupNavigationBase = {
  title : string
}

const Banner: FC<groupNavigationBase> = ({ title }) => {
    return (
        <BaseBanner >
            <IntroRaw title={title}/>
        </BaseBanner>
    );
};

export default Banner;
