import { styled as styledMu } from '@mui/material';
import Box from '@mui/material/Box';
import {
    bannerIcon,
} from '../../../../assets';

export const BaseBanner = styledMu(Box)`
    background: linear-gradient(#167af4, #00c1de);
    position:relative;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    height: 300px;
    width: 100%;
    background-size: auto 100%;
    background-position: right center;
    background-repeat: no-repeat;
    z-index:1;
    &:after {
        content: "";
        width:300px;
        height: 300px;
        position:absolute;
        right:0;
        top:0;
        background: url(${bannerIcon});
        background-repeat:no-repeat;
        z-index:-1;
        background-size: contain;
    }
`;
