import React, { FC } from 'react';
import { ContentWrapper } from './styles';
import Banner from './content/banner/banner';
import MainContent from './content/main/content';
import { useGetLearnTopicQuery } from '../../__data__/services/api';
import { useParams } from 'react-router-dom';

const AsideComponent: FC = () => {
    const { topic } = useParams();
    const { data, isLoading } = useGetLearnTopicQuery(topic);

    if (isLoading && !data) {
        return <div>Loading...</div>;
    }

    return (
        <ContentWrapper>
            <Banner title={data?.title} />
            <MainContent contentChapters={data?.chapters} overview={data?.overview} title={data?.title} />
        </ContentWrapper>
    );
};

export default AsideComponent;
