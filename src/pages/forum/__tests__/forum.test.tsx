/**
 * @jest-environment jsdom
 * @jest-environment-options {"url": "http://localhost:8099/coder/forum"}
 */
import React from 'react';
import { rest } from 'msw';
import { describe, expect, it, beforeAll, afterEach, afterAll } from '@jest/globals';
import { setupServer } from 'msw/node';
import { waitFor, screen, fireEvent } from '@testing-library/react';
import 'whatwg-fetch';

import ForumPage from '../forum';
import { render } from '../../../utils/test-utils';
import { ErrorBoundary } from '../../../components';

const server = setupServer(
    rest.get('/api/forum/topic-categories', (req, res, ctx) => {
        return res(
            ctx.json({
                success: true,
                body: [
                    {
                        id: 1,
                        title: 'Interview Question',
                        description: 'Share and discuss questions from real technical interviews',
                        path: 'interview-question',
                    },
                    {
                        id: 2,
                        title: 'Interview Experience',
                        description: 'Share details about the interview processes you have been through',
                        path: 'interview-experience',
                    },
                ],
                errors: [],
                warnings: [],
            }),
        );
    }),
);

// использовать глобальный чтобы правильно подгрузил модуль и не внутри теста
const mockedUseNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: () => mockedUseNavigate,
}));

// eslint-disable-next-line react/display-name
jest.mock('lottie-react', () => () => <div>Lottie is here</div>);

describe('Test Forum page', () => {
    beforeAll(() => {
        server.listen();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    afterAll(() => {
        server.close();
    });

    it('check render', async () => {
        const { container } = render(<ForumPage />);
        expect(container).toMatchSnapshot();
    });

    it('check to redirect', async () => {
        const { container } = render(<ForumPage />);
        await waitFor(() => expect(mockedUseNavigate).toBeCalledTimes(1));
        expect(container).toMatchSnapshot();
    });

    it('check to banner render', async () => {
        server.use(
            rest.get('/api/forum/topic-categories', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: false,
                        body: [],
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<ForumPage />);
        await waitFor(() => screen.getByText('Что-то пошло не так...'));
        expect(container).toMatchSnapshot();
    });

    it('check error boundary', async () => {
        const { container } = render(<ErrorBoundary>Error</ErrorBoundary>);
        await waitFor(() => expect(screen.getByText('Error')));
        expect(container).toMatchSnapshot();
    });

    it('check to change lang to EN handler', async () => {
        const { container } = render(<ForumPage />);
        await waitFor(() => screen.getByLabelText('EN'));
        fireEvent.click(screen.getByLabelText('EN'));
        expect(container).toMatchSnapshot();
    });

    it('check to change lang to RU handler', async () => {
        const { container } = render(<ForumPage />);
        await waitFor(() => screen.getByLabelText('RU'));
        fireEvent.click(screen.getByLabelText('RU'));
        expect(container).toMatchSnapshot();
    });

    it('check to auth sign up handler', async () => {
        const { container } = render(<ForumPage />);
        await waitFor(() => screen.getByLabelText('Регистрация'));
        fireEvent.click(screen.getByLabelText('Регистрация'));
        expect(container).toMatchSnapshot();
    });

    it('check to auth sign in handler', async () => {
        const { container } = render(<ForumPage />);
        await waitFor(() => screen.getByLabelText('Войти'));
        fireEvent.click(screen.getByLabelText('Войти'));
        expect(container).toMatchSnapshot();
    });
});
