import React, { FC, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { createUrl } from '../../__data__/constants';
import { useGetForumTopicCategoriesQuery } from '../../__data__/services/api';
import { SkeletonForum } from '../../components/skeletons';
import { Banner } from '../../components';
import { useTranslation } from 'react-i18next';

const ForumPage: FC = () => {
    const { data, isLoading } = useGetForumTopicCategoriesQuery(undefined);
    const navigate = useNavigate();
    const { t } = useTranslation();

    useEffect(() => {
        if (data && data.success) {
            const url = createUrl('link.coder.forum.group', { groupId: data?.body[0].path });
            navigate(url, { replace: true });
        }
    });

    if (isLoading) {
        return <SkeletonForum />;
    }

    if (!data || !data.success) {
        return <Banner message={t('something_went_wrong')} />;
    }

    return null;
};

export default ForumPage;
