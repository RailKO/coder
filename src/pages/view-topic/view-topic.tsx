import { Box, Stack, Typography, Divider } from '@mui/material';
import React, { FC } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Button } from './view-topic.styles';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { changeToKilo, unixToDate } from '../../utils/utils';
import { useGetForumTopicQuery } from '../../__data__/services/api';
import { Banner, UserInfo, VoteChanger } from '../../components';
import { Comments, TopicViewer } from '../../widgets';
import { useTranslation } from 'react-i18next';
import { SkeletonViewTopic } from '../../components/skeletons';

type PropsType = {};

const ViewTopicPage: FC<PropsType> = (props) => {
    const params = useParams();
    const { data, isLoading } = useGetForumTopicQuery(params.topicId);
    const { t } = useTranslation();
    const navigate = useNavigate();

    if (isLoading) {
        return <SkeletonViewTopic />;
    }

    if (!data.success) {
        return <Banner message={t('something_went_wrong')} />;
    }

    return (
        <Box bgcolor="white" borderRadius={1} mb={1}>
            <Stack direction="row" divider={<Divider orientation="vertical" flexItem />} p={1} spacing={1} borderBottom={1} borderColor="grey.300">
                <Button variant="outlined" startIcon={<ArrowBackIosIcon />} color="secondary" onClick={() => navigate(-1)}>
                    {t('back')}
                </Button>
                <Typography variant="h6">{data.body.title}</Typography>
            </Stack>
            <Stack direction="row">
                <Box>
                    <VoteChanger voteCount={data.body.voteCount} onVoteChange={() => {}} />
                </Box>
                <Box>
                    <UserInfo
                        user={data.body.author}
                        info={`${t('last_edit')} ${unixToDate(data.body.updationDate)}
                        \xa0\xa0\xa0\xa0${changeToKilo(data.body.viewCount)} ${t('VIEWS')}`}
                    />
                    <TopicViewer content={data.body.content} />
                </Box>
            </Stack>
            <Box>
                <Comments topicId={Number(params.topicId)} />
            </Box>
        </Box>
    );
};

export default ViewTopicPage;
