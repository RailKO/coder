import React from 'react';
import { rest } from 'msw';
import { describe, expect, it, beforeAll, afterEach, afterAll } from '@jest/globals';
import { setupServer } from 'msw/node';
import { waitFor, screen, fireEvent } from '@testing-library/react';
import 'whatwg-fetch';

import ViewTopicPage from '../view-topic';
import { render } from '../../../utils/test-utils';

const server = setupServer(
    rest.get('/api/forum/topic/:topicId', (req, res, ctx) => {
        return res(
            ctx.json({
                success: true,
                body: {
                    id: 1,
                    categoryId: 1,
                    title: 'How to write an Interview Question post',
                    commentCount: 2,
                    viewCount: 0,
                    tagsId: [1, 2],
                    voteCount: 0,
                    voteStatus: 0,
                    content: 'some text',
                    updationDate: 1648229493,
                    creationDate: 1524865315,
                    author: {
                        name: 'name',
                        avatar: 'avatar',
                    },
                    isOwnPost: true,
                },
                errors: [],
                warnings: [],
            }),
        );
    }),

    rest.get('/api/forum/topic-comments/:topicId', (req, res, ctx) => {
        return res(
            ctx.json({
                success: true,
                body: {
                    page: 1,
                    pageSize: 2,
                    total: 2,
                    totalPages: 1,
                    comments: [
                        {
                            id: 1,
                            topicId: 1,
                            voteCount: 36,
                            content: 'wish for offer',
                            updationDate: 1563064458,
                            creationDate: 1563064458,
                            author: {
                                name: 'name',
                                avatar: 'avatar',
                            },
                            authorIsModerator: false,
                            isOwnPost: false,
                        },
                    ],
                },
                errors: [],
                warnings: [],
            }),
        );
    }),

    rest.post('/api/forum/topic-comments', (req, res, ctx) => {
        return res(ctx.json({ success: true, body: {}, errors: [], warnings: [] }));
    }),
);

// использовать глобальный чтобы правильно подгрузил модуль и не внутри теста
const mockedUseNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useParams: () => ({ topicId: '1' }),
    useNavigate: () => mockedUseNavigate,
}));

// eslint-disable-next-line react/display-name
jest.mock('lottie-react', () => () => <div>Lottie is here</div>);

describe('Test NewTopic page', () => {
    beforeAll(() => {
        server.listen();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    afterAll(() => {
        server.close();
    });

    it('check render skeleton', async () => {
        const { container } = render(<ViewTopicPage />);
        expect(container).toMatchSnapshot();
    });

    it('check render page', async () => {
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByText('Назад'));
        expect(container).toMatchSnapshot();
    });

    it('check render banner', async () => {
        server.use(
            rest.get('/api/forum/topic/:topicId', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: false,
                        body: [],
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByText('Что-то пошло не так...'));
        expect(container).toMatchSnapshot();
    });

    it('check back button click', async () => {
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByText('Назад'));
        fireEvent.click(screen.getByText('Назад'));
        expect(mockedUseNavigate).toBeCalledTimes(1);
        expect(container).toMatchSnapshot();
    });

    it('check render comments banner', async () => {
        server.use(
            rest.get('/api/forum/topic-comments/:topicId', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: false,
                        body: [],
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByText('Что-то пошло не так...'));
        expect(container).toMatchSnapshot();
    });

    it('check render comments widget', async () => {
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByText('wish for offer'));
        expect(container).toMatchSnapshot();
    });

    it('check send comment handler', async () => {
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByText('Отправить'));
        fireEvent.click(screen.getByText('Отправить'));
        expect(container).toMatchSnapshot();
    });

    it('check comment change callback', async () => {
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByText('Отправить'));
        fireEvent.change(screen.getByPlaceholderText('Введите комментарий здесь...'), { target: { value: 'a' } });
        expect(container).toMatchSnapshot();
    });

    it('check to comment with different creation and updation date', async () => {
        server.use(
            rest.get('/api/forum/topic-comments/:topicId', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: true,
                        body: {
                            page: 1,
                            pageSize: 2,
                            total: 2,
                            totalPages: 1,
                            comments: [
                                {
                                    id: 1,
                                    topicId: 1,
                                    voteCount: 36,
                                    content: 'wish for offer',
                                    updationDate: 1563064458,
                                    creationDate: 1563064459,
                                    author: {
                                        name: 'name',
                                        avatar: 'avatar',
                                    },
                                    authorIsModerator: false,
                                    isOwnPost: false,
                                },
                            ],
                        },
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByText('wish for offer'));
        expect(container).toMatchSnapshot();
    });

    it('check vote changer up change callback', async () => {
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByLabelText('up vote'));
        fireEvent.click(screen.getByLabelText('up vote'));
        expect(container).toMatchSnapshot();
    });

    it('check vote changer down change callback', async () => {
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByLabelText('down vote'));
        fireEvent.click(screen.getByLabelText('down vote'));
        expect(container).toMatchSnapshot();
    });

    it('check to comment when author is null', async () => {
        server.use(
            rest.get('/api/forum/topic-comments/:topicId', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: true,
                        body: {
                            page: 1,
                            pageSize: 2,
                            total: 2,
                            totalPages: 1,
                            comments: [
                                {
                                    id: 1,
                                    topicId: 1,
                                    voteCount: 36,
                                    content: 'wish for offer',
                                    updationDate: 1563064458,
                                    creationDate: 1563064459,
                                    author: null,
                                    authorIsModerator: false,
                                    isOwnPost: false,
                                },
                            ],
                        },
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<ViewTopicPage />);
        await waitFor(() => screen.getByText('wish for offer'));
        expect(container).toMatchSnapshot();
    });
});
