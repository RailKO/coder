/**
 * @jest-environment jsdom
 * @jest-environment-options {"url": "http://localhost:8099/coder/forum/interview-question"}
 */
import React from 'react';
import { rest } from 'msw';
import { describe, expect, it, beforeAll, afterEach, afterAll } from '@jest/globals';
import { setupServer } from 'msw/node';
import { waitFor, screen, fireEvent } from '@testing-library/react';
import 'whatwg-fetch';

import TopicListPage from '../topic-list';
import { render } from '../../../utils/test-utils';

const server = setupServer(
    rest.get('/api/forum/topic-categories/:groupId', (req, res, ctx) => {
        return res(
            ctx.json({
                success: true,
                body: {
                    id: 1,
                    title: 'Interview Question',
                    description: 'Share and discuss questions from real technical interviews',
                    path: 'interview-question',
                },
                errors: [],
                warnings: [],
            }),
        );
    }),
    rest.get('/api/forum/topic-categories', (req, res, ctx) => {
        return res(
            ctx.json({
                success: true,
                body: [
                    {
                        id: 1,
                        title: 'Interview Question',
                        description: 'Share and discuss questions from real technical interviews',
                        path: 'interview-question',
                    },
                    {
                        id: 2,
                        title: 'Interview Experience',
                        description: 'Share details about the interview processes you have been through',
                        path: 'interview-experience',
                    },
                ],
                errors: [],
                warnings: [],
            }),
        );
    }),
    rest.get('/api/forum/topic-tags', (req, res, ctx) => {
        return res(
            ctx.json({
                success: true,
                body: [
                    {
                        id: 1,
                        name: 'google',
                        numTopics: 0,
                    },
                ],
                errors: [],
                warnings: [],
            }),
        );
    }),
    rest.get('/api/forum/topic-list/:groupId', (req, res, ctx) => {
        return res(
            ctx.json({
                success: true,
                body: {
                    page: 1,
                    pageSize: 2,
                    total: 2,
                    totalPages: 1,
                    topics: [
                        {
                            id: 1,
                            categoryId: 1,
                            title: 'How to write an Interview Question post',
                            commentCount: 2,
                            viewCount: 0,
                            tagsId: [1, 2],
                            voteCount: 0,
                            voteStatus: 0,
                            content: 'some text',
                            updationDate: 1524865315,
                            creationDate: 1524865315,
                            author: {
                                name: 'name',
                                avatar: 'avatar',
                            },
                            isOwnPost: true,
                        },
                    ],
                },
                errors: [],
                warnings: [],
            }),
        );
    }),
);

// использовать глобальный чтобы правильно подгрузил модуль и не внутри теста
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useParams: () => ({ groupId: 'interview-question' }),
}));

// eslint-disable-next-line react/display-name
jest.mock('lottie-react', () => () => <div>Lottie is here</div>);

describe('Test TopicList page', () => {
    beforeAll(() => {
        server.listen();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    afterAll(() => {
        server.close();
    });

    it('check render skeleton', async () => {
        const { container } = render(<TopicListPage />);
        expect(container).toMatchSnapshot();
    });

    it('check render page', async () => {
        const { container } = render(<TopicListPage />);
        await waitFor(() => screen.getByText('How to write an Interview Question post'));
        await waitFor(() => screen.getByPlaceholderText('Поиск тегов...'));
        expect(container).toMatchSnapshot();
    });

    it('check render banner', async () => {
        server.use(
            rest.get('/api/forum/topic-categories/:groupId', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: false,
                        body: [],
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<TopicListPage />);
        await waitFor(() => screen.getByText('Что-то пошло не так...'));
        expect(container).toMatchSnapshot();
    });

    it('check render categories banner', async () => {
        server.use(
            rest.get('/api/forum/topic-categories', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: false,
                        body: [],
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<TopicListPage />);
        await waitFor(() => screen.getByText('Категории не загружаются, попробуйте обновить страницу'));
        expect(container).toMatchSnapshot();
    });

    it('check render topic list banner', async () => {
        server.use(
            rest.get('/api/forum/topic-list/:groupId', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: false,
                        body: [],
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<TopicListPage />);
        await waitFor(() => screen.getByText('Темы не загружаются, попробуйте обновить страницу'));
        expect(container).toMatchSnapshot();
    });

    it('check tag search handler', async () => {
        const { container } = render(<TopicListPage />);
        await waitFor(() => screen.getByPlaceholderText('Поиск тегов...'));
        fireEvent.change(screen.getByPlaceholderText('Поиск тегов...'), { target: { value: 'a' } });
        expect(container).toMatchSnapshot();
    });

    it('check sorting handler', async () => {
        const { container } = render(<TopicListPage />);
        await waitFor(() => screen.getByPlaceholderText('Поиск тегов...'));
        await waitFor(() => screen.getByLabelText('topics sorting'));
        fireEvent.click(screen.getByLabelText('topics sorting'));
        expect(container).toMatchSnapshot();
    });

    it('check when author is null', async () => {
        server.use(
            rest.get('/api/forum/topic-list/:groupId', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: true,
                        body: {
                            page: 1,
                            pageSize: 2,
                            total: 2,
                            totalPages: 1,
                            topics: [
                                {
                                    id: 1,
                                    categoryId: 1,
                                    title: 'How to write an Interview Question post',
                                    commentCount: 2,
                                    viewCount: 0,
                                    tagsId: [1, 2],
                                    voteCount: 0,
                                    voteStatus: 0,
                                    content: 'some text',
                                    updationDate: 1524865315,
                                    creationDate: 1524865315,
                                    author: null,
                                    isOwnPost: true,
                                },
                            ],
                        },
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<TopicListPage />);
        await waitFor(() => screen.getByText('How to write an Interview Question post'));
        expect(container).toMatchSnapshot();
    });
});
