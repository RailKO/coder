import { Box, Stack } from '@mui/material';
import React, { FC } from 'react';
import { useParams } from 'react-router-dom';
import { Banner } from '../../components';
import { SkeletonForum } from '../../components/skeletons';
import { Categories, Tags, Topics } from '../../widgets';
import { useGetForumTopicCategoryByPathQuery } from '../../__data__/services/api';
import { useTranslation } from 'react-i18next';

const TopicListPage: FC = () => {
    const { groupId } = useParams();
    const { data, isLoading } = useGetForumTopicCategoryByPathQuery(groupId);
    const { t } = useTranslation();

    if (isLoading) {
        return <SkeletonForum />;
    }

    if (!data || !data.success) {
        return <Banner message={t('something_went_wrong')} />;
    }

    return (
        <Box m={1}>
            <Categories />
            <Stack direction="row" spacing={1}>
                <Box flexShrink={1} flexGrow={1}>
                    <Topics currentCategory={data.body} />
                </Box>
                <Box flexShrink={0} flexGrow={0} width="230px">
                    <Tags />
                </Box>
            </Stack>
        </Box>
    );
};

export default TopicListPage;
