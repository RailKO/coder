import React, { FC } from 'react';
import { Container, Box, Typography } from '@mui/material';

const ResetPassMessageComponent: FC = () => {
    return (
        <Container component="main" maxWidth="xs">
            <Box
                sx={{ mt: 1, p: 2, border: '1px dashed grey', borderRadius: '10px' }}
            >
                <Typography fontFamily="Roboto" fontStyle="normal" variant="h4">
          Password Reset
                </Typography>
                <Box
                    component="hr"
                    sx={{ mt: 1, mb: 1, borderTop: '1px solid #ccc' }}
                ></Box>
                <Typography fontFamily="Roboto" variant="h6">
          We have sent you an e-mail. Please contact us if you do not receive it
          within a few minutes
                </Typography>
            </Box>
        </Container>
    );
};

export default ResetPassMessageComponent;
