export { default as CatalogComponent } from './catalog';
export { default as ForumPage } from './forum/forum';
export { default as NewTopicPage } from './new-topic/new-topic';
export { default as NotFoundPage } from './not-found/not-found';
export { default as TopicListPage } from './topic-list/topic-list';
export { default as ViewTopicPage } from './view-topic/view-topic';

export { default as AuthSignUp } from './AuthSignUp/sign-up';
export { default as AuthSignIn } from './AuthSignIn/index';
export { default as AuthPasswRstLogged } from './AuthPasswRstLogged';
export { default as AuthPasswRst } from './AuthPasswRst/index';
export { default as AuthPasswMessage } from './AuthPasswMessage/index';

