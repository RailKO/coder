import React, { FC } from 'react';
import { URLS } from 'src/__data__/constants';
import { Container, Box, TextField, Typography } from '@mui/material';
import { Button } from './styles';

const ResetPassComponent: FC = () => {
    return (
        <Container component="main" maxWidth="xs">
            <Box
                component="form"
                noValidate
                sx={{ mt: 1, p: 2, border: '1px dashed grey', borderRadius: '10px' }}
            >
                <Typography fontFamily="Roboto" fontStyle="normal" variant="h4">
          Password Reset
                </Typography>
                <Box
                    component="hr"
                    sx={{ mt: 1, mb: 1, borderTop: '1px solid #ccc' }}
                ></Box>
                <Typography fontFamily="Roboto" variant="h6">
          Forgotten your password? Enter your e-mail address below, and we ll
          send you an e-mail allowing you to reset it.
                </Typography>
                <TextField
                    required
                    fullWidth
                    label="E-mail address"
                    color="secondary"
                    margin="normal"
                />
                <Button href={URLS.auth.urlResetPasswMessage} sx={{ mt: 2, mb: 2 }}>
          Reset My Password
                </Button>
            </Box>
        </Container>
    );
};

export default ResetPassComponent;
