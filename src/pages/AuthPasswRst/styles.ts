import { Button as MuiButton, styled } from '@mui/material';

export const Button = styled(MuiButton)`
  position: relative;
  width: 150px;
  height: 40px;
  background: #435862;
  color: #fff;
  border: 1px solid #000000;
  box-sizing: border-box;
  border-radius: 5px;
  :hover {
    background: #4d6570;
  }
  font-size: 10px;
`;
