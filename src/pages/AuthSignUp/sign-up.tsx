import React, { FC } from 'react';
import { URLS } from 'src/__data__/constants';
import { logoIcon } from 'src/assets';
import {
    Container,
    Box,
    Grid,
    CardMedia,
    TextField,
    Typography,
} from '@mui/material';
import { Button, LinkMui } from './styles';
// import { useGetAuthQuery } from 'src/__data__/services/api';

const SignUpComponent: FC = () => {
    // const { isLoading } = useGetAuthQuery(undefined);
    // if (isLoading ) {
    //     return <div> LOADING......SignUpComponent</div>;
    // };
    return (
        <Container component="main" maxWidth="xs">
            <Grid container justifyContent="center">
                <CardMedia
                    sx={{ maxWidth: 100, mt: 1 }}
                    component="img"
                    image={logoIcon}
                    alt="site logo"
                />
            </Grid>
            <Box component="form" noValidate sx={{ mt: 1 }}>
                <TextField
                    required
                    fullWidth
                    label="Username"
                    color="secondary"
                    margin="normal"
                    autoFocus
                />
                <TextField
                    required
                    fullWidth
                    label="Password"
                    color="secondary"
                    margin="normal"
                />
                <TextField
                    required
                    fullWidth
                    label="Confirm password"
                    color="secondary"
                    margin="normal"
                />
                <TextField
                    required
                    fullWidth
                    label="E-mail address"
                    color="secondary"
                    margin="normal"
                />
                <Button fullWidth type="submit" sx={{ mt: 2, mb: 2 }}>
          Sign Up
                </Button>
                <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    spacing={2}
                >
                    <Grid item>
                        <Typography
                            fontFamily="Roboto"
                            fontStyle="normal"
                            color="#bdbdbd"
                            variant="h6"
                        >
              Have an account?
                        </Typography>
                    </Grid>
                    <Grid item>
                        <LinkMui href={URLS.auth.urlSignIn}>Sign In</LinkMui>
                    </Grid>
                </Grid>
            </Box>
        </Container>
    );
};

export default SignUpComponent;
