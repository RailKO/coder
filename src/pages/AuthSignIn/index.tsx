import React, { FC } from 'react';
import { URLS } from 'src/__data__/constants';
import { logoIcon } from 'src/assets';
import { Container, Box, Grid, CardMedia, TextField } from '@mui/material';
import { Button, LinkMui } from './styles';

const SignInComponent: FC = () => {
    return (
        <Container component="main" maxWidth="xs">
            <Grid container justifyContent="center">
                <CardMedia
                    sx={{ maxWidth: 100, mt: 1 }}
                    component="img"
                    image={logoIcon}
                    alt="site logo"
                />
            </Grid>
            <Box component="form" noValidate sx={{ mt: 1 }}>
                <TextField
                    required
                    fullWidth
                    label="Username or E-mail"
                    color="secondary"
                    margin="normal"
                    autoFocus
                />
                <TextField
                    required
                    fullWidth
                    label="Password"
                    color="secondary"
                    margin="normal"
                />
                <Button fullWidth type="submit" sx={{ mt: 2, mb: 2 }}>
          Sign In
                </Button>
                <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    spacing={4}
                >
                    <Grid item>
                        <LinkMui href={URLS.auth.urlResetPassw}>Forgot Password?</LinkMui>
                    </Grid>
                    <Grid item>
                        <LinkMui href={URLS.auth.urlSignUp}>Sign Up</LinkMui>
                    </Grid>
                </Grid>
            </Box>
        </Container>
    );
};

export default SignInComponent;
