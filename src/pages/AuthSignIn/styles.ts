import { Button as MuiButton, Link, styled } from '@mui/material';

export const Button = styled(MuiButton)`
  background: #435862;
  color: #fff;
  border: 1px solid #000000;
  box-sizing: border-box;
  border-radius: 5px;
  :hover {
    background: #4d6570;
  }
`;

export const LinkMui = styled(Link)`
  text-decoration: none;
  cursor: pointer;
  color: #41555e;
  &:hover {
    color: #212121;
  }
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  font-size: 20px;
  line-height: 25px;
`;
