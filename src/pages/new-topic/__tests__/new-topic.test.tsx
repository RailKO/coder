import React from 'react';
import { rest } from 'msw';
import { describe, expect, it, beforeAll, afterEach, afterAll } from '@jest/globals';
import { setupServer } from 'msw/node';
import { waitFor, screen, fireEvent } from '@testing-library/react';
import 'whatwg-fetch';

import NewTopicPage from '../new-topic';
import { render } from '../../../utils/test-utils';

const server = setupServer(
    rest.get('/api/forum/topic-categories/:groupId', (req, res, ctx) => {
        return res(
            ctx.json({
                success: true,
                body: {
                    id: 1,
                    title: 'Interview Question',
                    description: 'Share and discuss questions from real technical interviews',
                    path: 'interview-question',
                },
                errors: [],
                warnings: [],
            }),
        );
    }),

    rest.post('/api/forum/topic', (req, res, ctx) => {
        return res(ctx.json({ success: true, body: 1, errors: [], warnings: [] }));
    }),

    rest.get('/api/forum/topic-tags', (req, res, ctx) => {
        return res(
            ctx.json({
                success: true,
                body: [
                    {
                        id: 1,
                        name: 'google',
                        numTopics: 0,
                    },
                ],
                errors: [],
                warnings: [],
            }),
        );
    }),
);

// использовать глобальный чтобы правильно подгрузил модуль и не внутри теста
const mockedUseNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useParams: () => ({ groupId: 'interview-question' }),
    useNavigate: () => mockedUseNavigate,
}));

// eslint-disable-next-line react/display-name
jest.mock('lottie-react', () => () => <div>Lottie is here</div>);

describe('Test NewTopic page', () => {
    beforeAll(() => {
        server.listen();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    afterAll(() => {
        server.close();
    });

    it('check render skeleton', async () => {
        const { container } = render(<NewTopicPage />);
        expect(container).toMatchSnapshot();
    });

    it('check render page', async () => {
        const { container } = render(<NewTopicPage />);
        await waitFor(() => screen.getByText('Отправить'));
        expect(container).toMatchSnapshot();
    });

    it('check render banner', async () => {
        server.use(
            rest.get('/api/forum/topic-categories/:groupId', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: false,
                        body: [],
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<NewTopicPage />);
        await waitFor(() => screen.getByText('Что-то пошло не так...'));
        expect(container).toMatchSnapshot();
    });

    it('check render banner handler', async () => {
        const { location } = window;
        delete (window as Partial<Window>).location;
        window.location = { ...window.location, reload: jest.fn() };
        server.use(
            rest.get('/api/forum/topic-categories/:groupId', (req, res, ctx) => {
                return res(
                    ctx.json({
                        success: false,
                        body: [],
                        errors: [],
                        warnings: [],
                    }),
                );
            }),
        );
        const { container } = render(<NewTopicPage />);
        await waitFor(() => screen.getByTitle('banner button'));
        fireEvent.click(screen.getByTitle('banner button'));
        expect(container).toMatchSnapshot();
        window.location = location;
    });

    it('check title change callback', async () => {
        const { container } = render(<NewTopicPage />);
        await waitFor(() => screen.getByText('Отправить'));
        fireEvent.change(screen.getByPlaceholderText('Введите название темы...'), { target: { value: 'a' } });
        expect(container).toMatchSnapshot();
    });

    it('check send button callback', async () => {
        const { container } = render(<NewTopicPage />);
        await waitFor(() => screen.getByText('Отправить'));
        fireEvent.click(screen.getByText('Отправить'));
        expect(container).toMatchSnapshot();
    });

    it('check cancel button click', async () => {
        const { container } = render(<NewTopicPage />);
        await waitFor(() => screen.getByText('Отмена'));
        fireEvent.click(screen.getByText('Отмена'));
        expect(mockedUseNavigate).toBeCalledTimes(1);
        expect(container).toMatchSnapshot();
    });

    it('check redirect to view added topic', async () => {
        mockedUseNavigate.mockClear();
        const { container } = render(<NewTopicPage />);
        await waitFor(() => screen.getByText('Отправить'));
        fireEvent.click(screen.getByText('Отправить'));
        await waitFor(() => expect(mockedUseNavigate).toBeCalledTimes(1));
        expect(container).toMatchSnapshot();
    });

    // it('check topic editor onChange callback', async () => {
    //     const { container } = render(<NewTopicPage />);
    //     await waitFor(() => screen.getByPlaceholderText('Пожалуйста, введите текст'));
    //     fireEvent.change(screen.getByPlaceholderText('Пожалуйста, введите текст'), { target: { value: 'a' } });
    //     expect(container).toMatchSnapshot();
    // });
});
