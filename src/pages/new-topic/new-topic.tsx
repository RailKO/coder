import React, { FC, useEffect, useState } from 'react';
import { Box, Stack, TextField } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import { Button } from './new-topic.styles';
import { useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { TagsPicker, TopicEditor } from '../../widgets';
import { ForumTopicToInsertType, TagType } from '../../typings/types';
import { useGetForumTopicCategoryByPathQuery, useInsertForumTopicMutation } from '../../__data__/services/api';
import { Banner } from '../../components';
import { SkeletonNewTopic } from '../../components/skeletons';
import { createUrl } from '../../__data__/constants';

const NewTopicPage: FC = () => {
    const { groupId } = useParams();
    const { data, isLoading } = useGetForumTopicCategoryByPathQuery(groupId);
    const [insertTopic, result] = useInsertForumTopicMutation();
    const [content, setContent] = useState('');
    const [title, setTitle] = useState('');
    const [tags, setTags] = useState([]);
    const navigate = useNavigate();
    const { t } = useTranslation();

    useEffect(() => {
        if (result.isSuccess) {
            if (result.data.success) {
                const url = createUrl('link.coder.forum.topic', { groupId, topicId: result.data.body });
                navigate(url);
            }
        }
    }, [result]);

    const onTitleChange = ({ target }) => {
        setTitle(target.value);
    };

    const onContentChange = (content: string) => {
        setContent(content);
    };

    const onTagsChanged = (value: Array<TagType>) => {
        setTags(value);
    };

    const onSendButtonHandler = () => {
        const topic: ForumTopicToInsertType = {
            categoryId: data.body.id,
            authorId: 1,
            content,
            tags,
            title,
        };
        insertTopic(topic);
    };

    if (isLoading) {
        return <SkeletonNewTopic />;
    }

    if (!data || !data.success) {
        return <Banner message={t('something_went_wrong')} />;
    }

    return (
        <Box bgcolor="white" p={1} borderRadius={1} mb={1}>
            <Stack mb={1} spacing={1} direction="row" justifyContent="space-between">
                <Box flexGrow={1}>
                    <TextField fullWidth placeholder={t('enter_topic_title')} value={title} onChange={onTitleChange} size="small" color="secondary" />
                </Box>
                <Button variant="outlined" color="secondary" onClick={() => navigate(-1)}>
                    {t('cancel')}
                </Button>
                <Button variant="contained" endIcon={<SendIcon />} color="secondary" onClick={onSendButtonHandler}>
                    {t('send')}
                </Button>
            </Stack>
            <TagsPicker getTags={onTagsChanged} />
            <TopicEditor initialContent={content} changeContent={onContentChange} />
        </Box>
    );
};

export default NewTopicPage;
