const locales = require('../locales/ru.json');

const t = (key) => locales[key];

module.exports = {
    t,
    useTranslation: () => ({
        t,
        i18n: {
            changeLanguage: () => {},
        },
    }),
};
