const { MongoClient } = require('mongodb');

// Connection URL
const url = 'mongodb://localhost:27017';

const getDB = async (name) => {
    try {
        const client = new MongoClient(url);
        await client.connect();
        return await client.db(name);
    } catch (error) {
        console.error(error);
    }
};

module.exports = {
    getDB,
};
