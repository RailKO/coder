// eslint-disable-next-line new-cap
const router = require('express').Router();

router.get('/catalog', (request, response, next) => {
    setTimeout(() => next());
}, (request, response) => {
    const params = process.env.stub;
    if (params === 'error') {
        response.send(require('./error.json'));
    } else {
        response.send(require('./cardData.json'));
    };
});

module.exports = router;
