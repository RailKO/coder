const router = require('express').Router();

router.get('/gettopic/:topic/:section', (request, response) => {
    const topic = request.params.topic;
    let section = request.params.section;

    if (!topic || !section) {
        response.send(require('./error.json'));
    } else {
        response.send(
            // require(`./${topic}/${section}.json`)
             require(`./dynamic-programming/${section}.json`)
        );
    }
});

router.get('/gettopic/:topic', (request, response) => {
    const topic = request.params.topic;

    if (!topic) {
        response.send(require('./error.json'));
    } else {
        response.send(
            //require(`./${topic}.json`)
            require(`./dynamic-programming.json`)
        );
    }
});

module.exports = router;