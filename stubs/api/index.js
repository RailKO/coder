// eslint-disable-next-line new-cap
const router = require('express').Router();


router.use(require('./forum'));
router.use(require('./catalog'));
router.use(require('./topic'));

module.exports = router;
