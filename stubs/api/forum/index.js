const { getResponse } = require('../../../utils/common');
const {
    getTags,
    connect,
    findTags,
    insertTag,
    getCategories,
    getTopicsByCategoryId,
    getTopicById,
    getCommentsByTopicId,
    insertComment,
    getCategoryByPath,
    insertTopic,
} = require('../controllers');

// eslint-disable-next-line new-cap
const router = require('express').Router();

connect();

router.get('/forum/topic-categories', async (req, res) => {
    const errors = [];
    const categories = await getCategories().catch((e) => errors.push(e.message));
    res.send(getResponse(errors, categories));
});

router.get('/forum/topic-categories/:groupId', async (req, res) => {
    const errors = [];
    const category = await getCategoryByPath(req.params.groupId).catch((e) => errors.push(e.message));
    res.send(getResponse(errors, category));
});

router.get('/forum/topic-comments/:topicId', async (req, res) => {
    const errors = [];
    const comments = await getCommentsByTopicId(req.params.topicId, req.query.page).catch((e) => errors.push(e.message));
    res.send(getResponse(errors, comments));
});

router.post('/forum/topic-comments', async (req, res) => {
    const errors = [];
    if (!req.body) {
        errors.push('Bad request, no body');
        res.send(getResponse(errors, undefined));
    } else {
        const comment = await insertComment(req.body).catch((e) => errors.push(e.message));
        res.send(getResponse(errors, comment));
    }
});

router.get('/forum/topic-list/:id', async (req, res) => {
    const errors = [];
    const topics = await getTopicsByCategoryId(req.params.id, req.query.page).catch((e) => errors.push(e.message));
    res.send(getResponse(errors, topics));
});

router.get('/forum/topic/:id', async (req, res) => {
    const errors = [];
    const topic = await getTopicById(req.params.id).catch((e) => errors.push(e.message));
    res.send(getResponse(errors, topic));
});

router.post('/forum/topic', async (req, res) => {
    const errors = [];
    if (!req.body) {
        errors.push('Bad request, no body');
        res.send(getResponse(errors, undefined));
    } else {
        const topic = await insertTopic(req.body).catch((e) => errors.push(e.message));
        res.send(getResponse(errors, topic));
    }
});

router.get('/forum/topic-tags', async (req, res) => {
    const errors = [];
    if (req.query.search !== undefined) {
        const tags = await findTags(req.query.search).catch((e) => errors.push(e.message));
        res.send(getResponse(errors, tags));
    } else {
        const tags = await getTags().catch((e) => errors.push(e.message));
        res.send(getResponse(errors, tags));
    }
});

router.post('/forum/topic-tags', async (req, res) => {
    const errors = [];
    const tags = await insertTag(req.body?.name).catch((e) => errors.push(e.message));
    res.send(getResponse(errors, tags));
});

router.get('/forum/stub/:data', (request, response) => {
    console.log(request.params);
    process.env.stub = request.params.data;

    response.sendStatus(200);
});

module.exports = router;
