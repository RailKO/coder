/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/configuration
 */

export default {
    collectCoverageFrom: [
        '**/src/**/*.{js,jsx,ts,tsx}',
        '!**/src/app.tsx',
        '!**/src/index.tsx',
        '!**/src/assets/**',
        '!**/src/components/index.ts',
        '!**/src/widgets/index.ts',
        '!**/src/pages/index.ts',
        '!**/src/typings/**',
        '!**/src/pages/AuthPasswMessage/**',
        '!**/src/pages/AuthPasswRst/**',
        '!**/src/pages/AuthPasswRstLogged/**',
        '!**/src/pages/AuthSignIn/**',
        '!**/src/pages/AuthSignUp/**',
        '!**/src/pages/detail/typing/**',
        '!**/src/__data__/constants/**',
    ],

    coverageDirectory: '<rootDir>/reports/coverage',

    coverageReporters: [['html', { subdir: 'html' }]],

    globals: {
        __webpack_public_path__: '',
    },

    moduleNameMapper: {
        '\\.(png|jpe?g|svg|css)$': '<rootDir>/__mocks__/file',
        'i18next': '<rootDir>/__mocks__/i18next.js',
        'react-i18next': '<rootDir>/__mocks__/i18next.js',
        '@ijl/cli': '<rootDir>/__mocks__/ijl-cli.js',
    },

    reporters: undefined,

    testEnvironment: 'jsdom',

    transform: {
        '^.+\\.[t|j]sx?$': 'babel-jest',
    },
};
