const pkg = require('./package');

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`,
        },
        // для возможности отладки на стенде, включаем только если сборка падает, потом отключаем
        // devtool: 'eval-source-map',
    },
    navigations: {
        'coder.main': '/coder',
        // Страница с топиками
        'link.coder.topics': '/coder/topics',
        // Сам топик
        'link.coder.topic': '/coder/topics/:topic',
        'link.coder.topic.section': 'coder/topics/:topic/:section',

        'link.coder.auth.sign-in': '/coder/auth/sign-in',
        'link.coder.auth.sign-up': '/coder/auth/sign-up',
        'link.coder.auth.reset-password': '/coder/auth/reset-password',
        'link.coder.auth.reset-password-message': '/coder/auth/reset-password-mesage',
        'link.coder.auth.reset-password-message-logged': '/coder/auth/reset-password-mesage-logged',

        'link.coder.forum': '/coder/forum',
        'link.coder.forum.group': '/coder/forum/:groupId',
        'link.coder.forum.topic': '/coder/forum/:groupId/topic/:topicId',
        'link.coder.forum.new.topic': '/coder/forum/:groupId/new-topic',
    },
    features: {
        coder: {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        'coder.api': '/api',
    },
};
